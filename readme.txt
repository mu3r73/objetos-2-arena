This is my OOP 2 Homewyrk.
It requires Java + Maven + the Arena MVVM framework.


Howto:

1. install maven plugin for eclipse

1.1. eclipse Neon already comes with m2e...

1.2. if it doesn't work, it might require these settings:
http://arena.uqbar-project.org/usage.html

(in arch-based distros, maven's settings file is ~/.m2/settings.xml)

1.3. eclipse config:
@ Window, Preferences, Maven
- Do not automatically update dependencies from remote repositories: ON
- Download artifact sources: ON
- Download artifact javadocs: ON
- Update Maven projects on startup: OFF
- Download repository index on startup: ON

2. start a new maven project

2.1. add the files in this repo thing

2.2. if you get dozens of errors, try this:
     right click on the project's name, maven, update project...
     check *only* these options:
       - update project config from pom.xml
       - refresh workspace resources from local filesystem
       - clean projects
     then hit ok
     (it may take a while to download all the required files,
      especially if your isp is speedy, and you live in a small town >:[
      - in my case, it downloaded 89M (including sources & javadoc;
        it should be ~41 MB without sources & javadoc)
      - good news is they only need to be fetched "once",
        since they aren't saved per project, but @ ~/.m2/repository)

2.3. still dozens of errors? now do this:
     right click on the projects's name, maven, update project...
     check *all* the options    
     then hit ok

3. run as java app, after adding this to arguments / vm arguments:
-Djava.system.class.loader=org.uqbar.apo.APOClassLoader
