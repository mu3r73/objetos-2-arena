package e02_contador;

import org.uqbar.commons.utils.Observable;

@Observable
public class Contador {

	protected int val = 0;
	protected String ultcom = "";
	
	public void reset() {
		this.val = 0;
		this.ultcom = "reset";
	}
	
	public void inc() {
		this.val++;
		this.ultcom = "incremento";
	}
	
	public void dec() {
		this.val--;
		this.ultcom = "decremento";
	}
	
	public int getVal() {
		return this.val;
	}
	
	public String getUltcom() {
		return this.ultcom;
	}
	
}
