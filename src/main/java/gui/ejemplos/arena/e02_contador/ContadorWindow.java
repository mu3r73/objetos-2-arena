package gui.ejemplos.arena.e02_contador;

import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.MainWindow;

import e02_contador.Contador;

@SuppressWarnings("serial")
public class ContadorWindow extends MainWindow<Contador> {

	// constructor
	public ContadorWindow() {
		
		super(new Contador());
		
	}

	@Override
	public void createContents(Panel mainPanel) {
		
		this.setTitle("Contador");
		
		mainPanel.setLayout(new VerticalLayout());

		new Label(mainPanel)
			.setText("valor actual");
		
		new Label(mainPanel)
			.bindValueToProperty("val");
		
		new Button(mainPanel)
			.setCaption("inc")
			.onClick(
					() -> this.getModelObject().inc()
			);
		
		new Button(mainPanel)
			.setCaption("dec")
			.onClick(
					() -> this.getModelObject().dec()
			);
		
		new Button(mainPanel)
			.setCaption("reset")
			.onClick(
					() -> this.getModelObject().reset()
			);
		
		new Label(mainPanel)
			.setText("último comando:");
		
		new Label(mainPanel)
			.bindValueToProperty("ultcom");
		
	}
	
}
