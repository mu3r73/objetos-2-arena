package gui.ejemplos.arena.e01_hello_world;

import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.MainWindow;

@SuppressWarnings("serial")
public class HelloWorldWindow extends MainWindow<String> {

	// constructor
	public HelloWorldWindow() {
		
		super("");

	}

	@Override
	public void createContents(Panel mainPanel) {
		
		this.setTitle("Hola mundo");

		mainPanel.setLayout(new ColumnLayout(1));
		
		new Label(mainPanel).setText("HELO");
		new Label(mainPanel).setText("MAIL FROM yo@acá");
		new Label(mainPanel).setText("RCPT TO mundo@allá");
		new Label(mainPanel).setText("DATA");
		new Label(mainPanel).setText("hola, mundo, ¿cómo estás?");
		
	}

}
