package p06_mercaderias.arena.primera_ventana;

import p06_mercaderias.ProductoComprado;
import p06_mercaderias.Renglon;

public class PrimeraVentanaApp {

	public static void main(String[] args) {
		
		ProductoComprado mayonesa = new ProductoComprado("mayonesa", 0.15, 1.5, 20);
		Renglon renglon = new Renglon(5, mayonesa);
		
		PrimeraVentana ventana = new PrimeraVentana(renglon);
		
		ventana.startApplication();
		
	}
	
}
