package p06_mercaderias.arena.primera_ventana;

import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.MainWindow;

import p06_mercaderias.Renglon;

@SuppressWarnings("serial")
public class PrimeraVentana extends MainWindow<Renglon> {

	// constructores
	
	public PrimeraVentana(Renglon model) {
		super(model);
	}

	@Override
	public void createContents(Panel mainPanel) {
		Label leyendaCosto = new Label(mainPanel);
		leyendaCosto.setText("Costo");
		Label valorCosto = new Label(mainPanel);
		valorCosto.bindValueToProperty("costo");
	}
	
}
