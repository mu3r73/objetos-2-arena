package p06_mercaderias.arena.pedido_ventana;

import org.uqbar.commons.utils.Observable;

import p06_mercaderias.Pedido;
import p06_mercaderias.Renglon;

@Observable
public class AccionesSobreRenglonController {

	private Pedido pedido;
	private String renglonElegido;
	private String cantRenglonElegido;
	
	// getters / setters
	
	public Pedido getPedido() {
		return this.pedido;
	}
	
	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	public String getRenglonElegido() {
		return this.renglonElegido;
	}
	
	public void setRenglonElegido(String renglon) {
		this.renglonElegido = renglon;
	}
	
	public String getCantRenglonElegido() {
		return this.cantRenglonElegido;
	}
	
	public void setCantRenglonElegido(String cant) {
		this.cantRenglonElegido = cant;
	}
	
	// actaulizaciones
	
	public void actualizarCantRenglonElegido() {
		if (this.esNumRenglonElegidoValido()) {
			this.cantRenglonElegido = String.valueOf(this.getRenglonActual().getCantidad());
		} else {
			this.cantRenglonElegido = "escribir número entre 1 y "
					+ String.valueOf(this.getPedido().getRenglones().size());
		}
	}

	// consultas
	
	public boolean esNumRenglonElegidoValido() {
		return ((this.indiceRenglonElegido() >= 0)
				&& (this.indiceRenglonElegido() <= this.getPedido().getRenglones().size() - 1));
	}

	private int indiceRenglonElegido() {
		return Integer.parseInt(this.getRenglonElegido()) - 1;
	}

	public Renglon getRenglonActual() {
		return this.getPedido().getRenglones().get(this.indiceRenglonElegido());
	}

}
