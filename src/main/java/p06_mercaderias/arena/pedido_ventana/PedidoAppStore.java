package p06_mercaderias.arena.pedido_ventana;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import org.uqbar.commons.utils.Observable;

import p06_mercaderias.Producto;
import p06_mercaderias.ProductoComprado;

// singleton
@Observable
public class PedidoAppStore {

	private Collection<Producto> productos;
	private static PedidoAppStore instancia;
	
	// constructores
	
	private PedidoAppStore() {
		super();
	}

	// instancia singleton
	
	public static PedidoAppStore getInstancia() {
		if (instancia == null) {
			instancia = new PedidoAppStore();
		}
		return instancia;
	}
	
	// productos truchos
	
	public void cargarProductosEjemplo() {
		this.productos = new ArrayList<>();
		this.productos.add(new ProductoComprado("arroz", 0.5, 4, 45));
		this.productos.add(new ProductoComprado("atún", 0.5, 5, 50));
		this.productos.add(new ProductoComprado("arvejas", 0.5, 5, 25));
		this.productos.add(new ProductoComprado("porotos", 0.5, 6, 40));
		this.productos.add(new ProductoComprado("pan", 0.5, 5, 50));
	}
	
	// consultas
	
	public Producto getProducto(String nombre) {
		return this.productos.stream()
				.filter(producto -> producto.getNombre().equals(nombre))
				.findAny()
				.get();
	}
	
	public List<Producto> getProductosOrdenNombre() {
		return this.productos.stream()
				.sorted(Comparator.comparing(Producto::getNombre))
				.collect(Collectors.toList());
	}
	
}
