package p06_mercaderias.arena.pedido_ventana;

import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.List;
import org.uqbar.arena.widgets.NumericField;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Dialog;
import org.uqbar.arena.windows.WindowOwner;

import p06_mercaderias.Producto;
import p06_mercaderias.Renglon;

@SuppressWarnings("serial")
public class RenglonFormDialogo extends Dialog<Renglon> {

	public RenglonFormDialogo(WindowOwner owner, Renglon model) {
		super(owner, model);
	}

	@Override
	protected void createFormPanel(Panel mainPanel) {
		
		this.setMinHeight(200);
		this.setTitle("edición de un renglón");
		
		this.agregarSelector(mainPanel);
		
		this.agregarCuadroCantidad(mainPanel);
		
		this.agregarBotonAceptar(mainPanel);
		this.agregarBotonCancelar(mainPanel);
		
		this.agregarDebugAcceptCancel();
		
	}

	private void agregarSelector(Panel mainPanel) {
		List<Producto> selector = new List<>(mainPanel);
		selector
			.setWidth(220)
			.setHeight(220)
			.bindValueToProperty("producto");
		selector
			.bindItems(new ObservableProperty<>(PedidoAppStore.getInstancia(), "productosOrdenNombre"))
			.setAdapter(new PropertyAdapter(Producto.class, "nombre"));
	}

	private void agregarCuadroCantidad(Panel mainPanel) {
		new NumericField(mainPanel)
			.bindValueToProperty("cantidad");
	}

	private void agregarBotonAceptar(Panel mainPanel) {
		new Button(mainPanel)
			.setCaption("aceptar")
			.onClick(() ->
				this.accept()
			);
	}

	private void agregarBotonCancelar(Panel mainPanel) {
		new Button(mainPanel)
			.setCaption("cancelar")
			.onClick(() ->
				this.cancel()
			);
	}

	private void agregarDebugAcceptCancel() {
		this.onAccept(() -> {
			System.out.println("acción aceptada");
		});
		this.onCancel(() -> {
			System.out.println("acción cancelada");
		});
	}
	
}
