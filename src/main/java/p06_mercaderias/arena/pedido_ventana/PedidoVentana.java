package p06_mercaderias.arena.pedido_ventana;

import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.NumericField;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.MainWindow;
import org.uqbar.commons.model.ObservableUtils;

import p06_mercaderias.Pedido;
import p06_mercaderias.Renglon;

@SuppressWarnings("serial")
public class PedidoVentana extends MainWindow<Pedido> {

	public PedidoVentana(Pedido model) {
		super(model);
	}

	@Override
	public void createContents(Panel mainPanel) {
		
		this.setTitle("un pedido");
		
		this.agregarTablaRenglones(mainPanel);
		
		this.agregarBotonAgregarRenglon(mainPanel);
		
		this.agregarDetalleRenglon(mainPanel);
		
		this.agregarPanelCostoTotal(mainPanel);
		
	}

	private void agregarTablaRenglones(Panel mainPanel) {
		Table<Renglon> tabla = new Table<>(mainPanel, Renglon.class);
		tabla
			.setNumberVisibleRows(10)
			.setWidth(500)
			.setHeight(500);
		tabla.bindItemsToProperty("renglones");

		new Column<Renglon>(tabla)
			.setTitle("cantidad")
			.setFixedSize(80)
			.bindContentsToProperty("cantidad");
		new Column<Renglon>(tabla)
			.setTitle("producto")
			.setFixedSize(120)
			.bindContentsToProperty("producto.nombre");
		new Column<Renglon>(tabla)
			.setTitle("costo")
			.setFixedSize(80)
			.bindContentsToProperty("costo");
	}
	
	private void agregarBotonAgregarRenglon(Panel mainPanel) {
		new Button(mainPanel)
			.setCaption("agregar renglón")
			.onClick(() ->
				this.openRenglonForm()
			);		
	}

	private void openRenglonForm() {
		Renglon nuevoRenglon = new Renglon();
		
		RenglonFormDialogo form = new RenglonFormDialogo(this, nuevoRenglon);
		form.onAccept(() -> {
			this.getModelObject().agregarRenglon(nuevoRenglon);
			ObservableUtils.firePropertyChanged(this.getModelObject(), "costo");
		});
		form.open();
	}

	private void agregarDetalleRenglon(Panel mainPanel) {
		AccionesSobreRenglonController controlador = new AccionesSobreRenglonController();
		Panel detalleRenglon = new Panel(mainPanel, controlador);
								// controlador es el modelo para detalleRenglon
		detalleRenglon.setLayout(new HorizontalLayout());
		controlador.setPedido(this.getModelObject());
		
		new Label(detalleRenglon)
			.setText("renglón:")
			.setWidth(60);
		
		new NumericField(detalleRenglon)
			.setWidth(40)
			.bindValueToProperty("renglonElegido");
		
		new Button(detalleRenglon)
			.setCaption("ver detalle")
			.onClick(() -> {
				controlador.actualizarCantRenglonElegido();
				if (controlador.esNumRenglonElegidoValido()) {
					this.openDetalleRenglon(controlador.getRenglonActual());
				}
			});
		
		new Label(detalleRenglon)
			.bindValueToProperty("cantRenglonElegido");
	}

	private void openDetalleRenglon(Renglon renglon) {
		new RenglonVentanaModal(this, renglon)
			.open();
	}

	private void agregarPanelCostoTotal(Panel mainPanel) {
		Panel costoTotal = new Panel(mainPanel);
		costoTotal.setLayout(new ColumnLayout(2));
		new Label(costoTotal).setText("costo total:");
		new Label(costoTotal).bindValueToProperty("costo");
	}

}
