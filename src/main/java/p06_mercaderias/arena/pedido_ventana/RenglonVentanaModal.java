package p06_mercaderias.arena.pedido_ventana;

import java.awt.Color;

import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;

import p06_mercaderias.Renglon;

@SuppressWarnings("serial")
public class RenglonVentanaModal extends Window<Renglon> {

	public RenglonVentanaModal(WindowOwner owner, Renglon model) {
		super(owner, model);
	}

	@Override
	public void createContents(Panel mainPanel) {
		this.setTitle("item de un pedido")
			.setMinHeight(200);
		
		// 2 columnas
		mainPanel.setLayout(new ColumnLayout(2));
		
		// nombre del producto
		new Label(mainPanel)
			.setText("producto")
			.setFontSize(12)
			.setForeground(Color.BLUE);
		
		new Label(mainPanel)
			.setFontSize(12)
			.setForeground(Color.RED.darker())
			.bindValueToProperty("nombreDelProducto");
		
		// cantidad
		new Label(mainPanel)
			.setText("cantidad")
			.setFontSize(12)
			.setForeground(Color.BLUE);
		
		new Label(mainPanel)
			.setFontSize(12)
			.setForeground(Color.RED.darker())
			.bindValueToProperty("cantidad");
		
		// costo
		new Label(mainPanel)
			.setText("costo")
			.setFontSize(12)
			.setForeground(Color.BLUE);
		
		new Label(mainPanel)
			.setFontSize(12)
			.setForeground(Color.RED.darker())
			.bindValueToProperty("costo");

	}

}
