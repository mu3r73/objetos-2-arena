package p06_mercaderias.arena.pedido_ventana;

import p06_mercaderias.Pedido;

public class PedidoVentanaApp {

	public static void main(String[] args) {
		
		PedidoAppStore pas = PedidoAppStore.getInstancia();
		pas.cargarProductosEjemplo();
		
		Pedido pedido = new Pedido();
		pedido.agregarRenglon(5, pas.getProducto("arvejas"));
		pedido.agregarRenglon(1, pas.getProducto("atún"));
		pedido.agregarRenglon(3, pas.getProducto("arroz"));
		
		PedidoVentana ventana = new PedidoVentana(pedido);
		ventana.startApplication();
		
	}
	
}
