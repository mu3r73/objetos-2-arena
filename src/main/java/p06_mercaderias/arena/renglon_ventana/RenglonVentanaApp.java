package p06_mercaderias.arena.renglon_ventana;

import p06_mercaderias.ProductoComprado;
import p06_mercaderias.Renglon;

public class RenglonVentanaApp {

	public static void main(String[] args) {
		
		ProductoComprado mayonesa = new ProductoComprado("mayonesa", 0.15, 1.5, 20);
		Renglon renglon = new Renglon(5, mayonesa);
		
		RenglonVentana ventana = new RenglonVentana(renglon);
		
		ventana.startApplication();
	}			

}
