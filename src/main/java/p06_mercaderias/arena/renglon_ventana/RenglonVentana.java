package p06_mercaderias.arena.renglon_ventana;

import java.awt.Color;

import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.MainWindow;

import p06_mercaderias.Renglon;

@SuppressWarnings("serial")
public class RenglonVentana extends MainWindow<Renglon> {

	public RenglonVentana(Renglon model) {
		super(model);
	}

	@Override
	public void createContents(Panel mainPanel) {
		this.setTitle("Item de un pedido")
			.setMinHeight(200);
		
		// 2 columnas
		mainPanel.setLayout(new ColumnLayout(2));
		
		Label productoLabel = new Label(mainPanel);
		productoLabel.setText("Producto");
		productoLabel.setFontSize(12)
					.setForeground(Color.BLUE);
		
		Label productoName = new Label(mainPanel);
		productoName.bindValueToProperty("nombreDelProducto");
		productoName.setFontSize(12)
					.setForeground(Color.RED.darker());
	
		Label cantidadLabel = new Label(mainPanel);
		cantidadLabel.setText("Cantidad");
		cantidadLabel.setFontSize(12)
					.setForeground(Color.BLUE);
		
		Label cantidad = new Label(mainPanel);
		cantidad.bindValueToProperty("cantidad");
		cantidad.setFontSize(12)
					.setForeground(Color.RED.darker());

		Label costoLabel = new Label(mainPanel);
		costoLabel.setText("Costo");
		costoLabel.setFontSize(12)
					.setForeground(Color.BLUE);
		
		Label costo = new Label(mainPanel);
		costo.bindValueToProperty("costo");
		costo.setFontSize(12)
					.setForeground(Color.RED.darker());

	}

}
