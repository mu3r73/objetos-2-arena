package p06_mercaderias;

import org.uqbar.commons.utils.Observable;

@Observable
public class Renglon {

	private int cantidad;
	private Producto producto;
	
	// constructores
	
	public Renglon() {
		super();
	}

	public Renglon(int cantidad, Producto producto) {
		super();
		this.cantidad = cantidad;
		this.producto = producto;
	}

	// getters / setters
	
	public int getCantidad() {
		return this.cantidad;
	}
	
	public void setCantidad(int cant) {
		this.cantidad = cant;
	}
	
	public Producto getProducto() {
		return this.producto;
	}
	
	public void setProducto(Producto producto) {
		this.producto = producto;
	}
	
	// consultas
	
	public String getNombreDelProducto() {
		return this.producto.getNombre();
	}
	
	public double getCosto() {
		return this.cantidad * this.getProducto().getCosto();
	}
	
}
