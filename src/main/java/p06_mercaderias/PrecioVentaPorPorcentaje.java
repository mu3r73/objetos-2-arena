package p06_mercaderias;

public class PrecioVentaPorPorcentaje implements CalculoPrecioVenta {
	// singleton

	private static PrecioVentaPorPorcentaje instancia;
	private static double coeficiente;
	
	// constructores
	
	private PrecioVentaPorPorcentaje() {
		super();
	}
	
	// getters / setters
	
	public static PrecioVentaPorPorcentaje getInstancia() {
		if (PrecioVentaPorPorcentaje.instancia == null) {
			PrecioVentaPorPorcentaje.instancia = new PrecioVentaPorPorcentaje();
		}
		return PrecioVentaPorPorcentaje.instancia;
	}
	
	public static double getCoeficiente() {
		return PrecioVentaPorPorcentaje.coeficiente;
	}

	public static void setCoeficiente(double coeficiente) {
		PrecioVentaPorPorcentaje.coeficiente = coeficiente;
	}
	
	// consultas

	@Override
	public double getPrecioVenta(Producto producto) {
		return producto.getCosto() * PrecioVentaPorPorcentaje.coeficiente;
	}

}
