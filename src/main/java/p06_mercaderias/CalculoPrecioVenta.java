package p06_mercaderias;

public interface CalculoPrecioVenta {
	// patrón strategy

	double getPrecioVenta(Producto producto);

}
