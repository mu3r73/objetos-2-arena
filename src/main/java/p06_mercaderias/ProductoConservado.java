package p06_mercaderias;

public class ProductoConservado extends ProductoDeProveedor {

	private int diasConservacion;
	private static double costoConservPorDiaPorKg;

	// constructores
	
	public ProductoConservado(String nombre, double pesoProm, double valorAlmacPorKg, double precioCompra) {
		super(nombre, pesoProm, valorAlmacPorKg, precioCompra);
	}
	
	// getters / setters
	
	public int getDiasConservacion() {
		return this.diasConservacion;
	}
	
	public void setDiasConservacion(int dias) {
		this.diasConservacion = dias;
	}
	
	public static double getCostoConservPorDiaPorKg() {
		return ProductoConservado.costoConservPorDiaPorKg;
	}
	
	public static void setCostoConservPorDiaPorKg(double costoConservPorDiaPorKg) {
		ProductoConservado.costoConservPorDiaPorKg = costoConservPorDiaPorKg;
	}
	
	// consultas
	
	@Override
	public double getCostoProduccion() {
		return super.getCostoProduccion()
				* (this.diasConservacion * this.pesoProm
						* ProductoConservado.costoConservPorDiaPorKg);
	}

}
