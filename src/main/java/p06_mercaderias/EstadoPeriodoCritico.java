package p06_mercaderias;

public class EstadoPeriodoCritico implements EstadoEstacional {

	@Override
	public double getPrecioVenta(Producto producto) {
		return PrecioVentaPorPorcentaje.getInstancia()
				.getPrecioVenta(producto);
	}

}
