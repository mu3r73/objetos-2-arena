package p06_mercaderias;

public class PrecioVentaEstacional implements CalculoPrecioVenta {

	private EstadoEstacional estado;

	// constructor
	
	public PrecioVentaEstacional(EstadoEstacional estado) {
		super();
		this.estado = estado;
	}

	// getters / setters
	
	public void setEstado(EstadoEstacional estado) {
		this.estado = estado;
	}
	
	// consultas
	
	@Override
	public double getPrecioVenta(Producto producto) {
		return estado.getPrecioVenta(producto);
	}

}
