package p06_mercaderias;

import org.uqbar.commons.utils.Observable;

@Observable
public class ProductoComprado extends ProductoDeProveedor {
	
	private static final double plusPorTrasladoAlDeposito = 0.2; // %
	
	// constructores
	
	public ProductoComprado(String nombre, double pesoProm, double valorAlmacPorKg, double precioCompra) {
		super(nombre, pesoProm, valorAlmacPorKg, precioCompra);
	}
	
	// consultas
	
	@Override
	public double getCostoAlmacenamiento() {
		return super.getCostoAlmacenamiento() * (1 + ProductoComprado.plusPorTrasladoAlDeposito);
	}
	
}
