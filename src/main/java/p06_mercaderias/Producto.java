package p06_mercaderias;

/**
 * 
 */
public abstract class Producto {

	private String nombre;
	protected double pesoProm; // en kg
	private double valorAlmacPorKg;
	private CalculoPrecioVenta calcPrecioVenta;
	
	// cosnstructores
	
	public Producto(String nombre, double pesoProm, double valorAlmacPorKg) {
		super();
		this.nombre = nombre;
		this.pesoProm = pesoProm;
		this.valorAlmacPorKg = valorAlmacPorKg;
	}

	// getters / setters
	
	public String getNombre() {
		return this.nombre;
	}
	
	public double getPesoProm() {
		return this.pesoProm;
	}
	
	public double getValorAlmacPorKg() {
		return this.valorAlmacPorKg;
	}
	
	public CalculoPrecioVenta getCalcPrecioVenta() {
		return this.calcPrecioVenta;
	}
	
	public void setCalcPrecioVenta(CalculoPrecioVenta calcPrecioVenta) {
		this.calcPrecioVenta = calcPrecioVenta;
	}
	
	// consultas
	
	public double getCosto() {
		return this.getCostoProduccion() + this.getCostoAlmacenamiento();
	}

	public abstract double getCostoProduccion();
	// template method
	
	public double getCostoAlmacenamiento() {
		return this.pesoProm * this.valorAlmacPorKg;
	}

	public boolean esDelicado() {
		return this.pesoProm < 5;
	}
	
	public double getPrecioVenta() {
		return this.calcPrecioVenta.getPrecioVenta(this);
	}

}
