package p06_mercaderias;

public class PrecioVentaLiquidacion implements CalculoPrecioVenta {
	// singleton

	private static PrecioVentaLiquidacion instancia;
	
	// constructor
	
	private PrecioVentaLiquidacion() {
		super();
	}
	
	// getters y setters
	
	public static PrecioVentaLiquidacion getInstancia() {
		if (PrecioVentaLiquidacion.instancia == null) {
			PrecioVentaLiquidacion.instancia = new PrecioVentaLiquidacion();
		}	
		return PrecioVentaLiquidacion.instancia;
	}
	
	// consultas
	
	@Override
	public double getPrecioVenta(Producto producto) {
		return producto.getCosto();
	}

}
