package p06_mercaderias;

public abstract class ProductoDeProveedor extends Producto {
	
	public double precioCompra;

	// constructores
	
	public ProductoDeProveedor(String nombre, double pesoProm, double valorAlmacPorKg, double precioCompra) {
		super(nombre, pesoProm, valorAlmacPorKg);
		this.precioCompra = precioCompra;
	}
	
	// getters / setters
	
	public double getPrecioCompra() {
		return this.precioCompra;
	}
	
	// consultas
	
	@Override
	public double getCostoProduccion() {
		return this.precioCompra;
	}
	
}
