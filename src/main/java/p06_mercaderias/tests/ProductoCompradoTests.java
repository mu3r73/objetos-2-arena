package p06_mercaderias.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class ProductoCompradoTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	
	@Test
	public void testCostoAlmacenamiento() {
		
		// costoAlmacenamiento = (pesoProm * valorAlmacPorKg) * 1.2
		double resEsperado = (opt.comprado.getPesoProm()
							  * opt.comprado.getValorAlmacPorKg())
							 * 1.2;
		
		assertEquals(resEsperado, opt.comprado.getCostoAlmacenamiento(), 0);
		
	}
	
	@Test
	public void testCostoProduccion() {
		
		// costoProduccion = precioCompra
		double resEsperado = opt.comprado.getPrecioCompra();
		
		assertEquals(resEsperado, opt.comprado.getCostoProduccion(), 0);
		
	}

	@Test
	public void testCosto() {
		
		// costo = costoProduccion + costoAlmacenamiento
		double resEsperado = opt.comprado.getCostoProduccion() + opt.comprado.getCostoAlmacenamiento();
		
		assertEquals(resEsperado, opt.comprado.getCosto(), 0);
		
	}
	
}
