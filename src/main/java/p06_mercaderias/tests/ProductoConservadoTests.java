package p06_mercaderias.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import p06_mercaderias.ProductoConservado;

public class ProductoConservadoTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	
	@Before
	public void setUp() {
		
		opt.init();

	}

	@Test
	public void testCostoAlmacenamiento() {
		
		// costoAlmacenamiento = pesoProm * valorAlmacPorKg
		double resEsperado = opt.conservado.getPesoProm() * opt.conservado.getValorAlmacPorKg();
		
		assertEquals(resEsperado, opt.conservado.getCostoAlmacenamiento(), 0);
		
	}

	@Test
	public void testCostoProduccion() {
		
		// costoProduccion = precioCompra * (diasConservacion * pesoProm * costoConservPorDiaPorKg)
		double resEsperado = opt.conservado.getPrecioCompra()
							* (opt.conservado.getDiasConservacion()
									* opt.conservado.getPesoProm()
									* ProductoConservado.getCostoConservPorDiaPorKg());
		
		assertEquals(resEsperado, opt.conservado.getCostoProduccion(), 0);
		
	}

	@Test
	public void testCosto() {
		
		// costo = costoProduccion + costoAlmacenamiento
		double resEsperado = opt.conservado.getCostoProduccion() + opt.conservado.getCostoAlmacenamiento();
		
		assertEquals(resEsperado, opt.conservado.getCosto(), 0);
				
	}
	
}
