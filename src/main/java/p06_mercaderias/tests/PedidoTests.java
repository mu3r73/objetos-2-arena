package p06_mercaderias.tests;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import p06_mercaderias.Pedido;
import p06_mercaderias.PrecioVentaLiquidacion;
import p06_mercaderias.PrecioVentaPorMontoFijo;
import p06_mercaderias.PrecioVentaPorPorcentaje;
import p06_mercaderias.Producto;

public class PedidoTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	Pedido pedido = new Pedido();
	
	@Before
	public void setUp() {
		
		opt.init();
		
		pedido.agregarRenglon(10, opt.comprado);
		pedido.agregarRenglon(15, opt.conservado);
		pedido.agregarRenglon(20, opt.fabricado);

	}
	
	@Test
	public void testCosto() {
		
		double resEsperado = 10 * opt.comprado.getCosto()
				+ 15 * opt.conservado.getCosto()
				+ 20 * opt.fabricado.getCosto();
		
		assertEquals(resEsperado, pedido.getCosto(), 0);
		
	}
	
	@Test
	public void testProductosDelicados() {
		
		pedido.agregarRenglon(5, opt.delicadoConservado);
		pedido.agregarRenglon(5, opt.delicadoFabricado);
		pedido.agregarRenglon(5, opt.delicadoComprado);
		
		List<Producto> resEsperado = new ArrayList<>(
				Arrays.asList(opt.delicadoComprado, opt.delicadoConservado,
						opt.delicadoFabricado));
		
		assertEquals(resEsperado, pedido.getProductosDelicados());
		
	}

	@Test
	public void testListaDeProductos() {
		
		List<Producto> resEsperado = new ArrayList<>(
				Arrays.asList(opt.comprado, opt.conservado, opt.fabricado));
		
		assertEquals(resEsperado, pedido.getProductos());
		
	}
	
	@Test
	public void testPrecioVenta_Liquidacion() {
		
		double resEsperado = opt.comprado.getCosto();
		
		opt.comprado.setCalcPrecioVenta(PrecioVentaLiquidacion.getInstancia());
		
		assertEquals(resEsperado, opt.comprado.getPrecioVenta(), 0);
		
	}
	
	@Test
	public void testPrecioVenta_PorPorcentaje() {
		
		PrecioVentaPorPorcentaje.setCoeficiente(1.6);
		
		// costo * coeficiente
		double resEsperado = opt.comprado.getCosto()
				* PrecioVentaPorPorcentaje.getCoeficiente();
		
		opt.comprado.setCalcPrecioVenta(PrecioVentaPorPorcentaje.getInstancia());
		
		assertEquals(resEsperado, opt.comprado.getPrecioVenta(), 0);
		
	}
	
	@Test
	public void testPrecioVenta_PorMontoFijo() {
		
		opt.comprado.setCalcPrecioVenta(new PrecioVentaPorMontoFijo(12.34));
		
		// costo + montoFijo
		double resEsperado = opt.comprado.getCosto() + 12.34;
		
		assertEquals(resEsperado, opt.comprado.getPrecioVenta(), 0);
		
	}
	
}
