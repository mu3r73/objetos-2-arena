package p06_mercaderias.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import p06_mercaderias.ProductoFabricado;

public class ProductoFabricadoTests {

	private ObjetosParaTests opt = new ObjetosParaTests(); 

	@Before
	public void setUp() {
		
		opt.init();

	}
	
	@Test
	public void testCostoAlmacenamiento() {
		
		// costoAlmacenamiento = pesoProm * valorAlmacPorKg
		double resEsperado = opt.fabricado.getPesoProm() * opt.fabricado.getValorAlmacPorKg();
		
		assertEquals(resEsperado, opt.fabricado.getCostoAlmacenamiento(), 0);
		
	}

	@Test
	public void testCostoProduccion() {

		// costoProduccion = cantHorasHombre * costoHoraHombre
		double resEsperado = opt.fabricado.getCantHorasHombre()
							* ProductoFabricado.getCostoHoraHombre();
		
		assertEquals(resEsperado, opt.fabricado.getCostoProduccion(), 0);
		
	}

	@Test
	public void testCosto() {
		
		// costo = costoProduccion + costoAlmacenamiento
		double resEsperado = opt.fabricado.getCostoProduccion() + opt.fabricado.getCostoAlmacenamiento();
		
		assertEquals(resEsperado, opt.fabricado.getCosto(), 0);
		
	}

}
