package p06_mercaderias.tests;

import p06_mercaderias.ProductoComprado;
import p06_mercaderias.ProductoConservado;
import p06_mercaderias.ProductoFabricado;

public class ObjetosParaTests {

	// ProductoComprado(String nombre, double pesoProm, double valorAlmacPorKg, double precioCompra)
	public ProductoComprado comprado = new ProductoComprado("compr", 5.5, 2.5, 3.5);
	public ProductoComprado delicadoComprado = new ProductoComprado("compr", 1.5, 2.5, 3.5);

	// ProductoConservado(String nombre, double pesoProm, double valorAlmacPorKg, double precioCompra)
	public ProductoConservado conservado = new ProductoConservado("cons", 5.5, 2.5, 3.5);
	public ProductoConservado delicadoConservado = new ProductoConservado("cons", 2.5, 2.5, 3.5);
	
	// ProductoFabricado(String nombre, double pesoProm, double valorAlmacPorKg, double cantHorasHombre)
	public ProductoFabricado fabricado = new ProductoFabricado("fabr", 6.5, 5.5, 6.5);	
	public ProductoFabricado delicadoFabricado = new ProductoFabricado("fabr", 3.5, 5.5, 6.5);	
	
	// inicializacion
	public void init() {
		
		ProductoConservado.setCostoConservPorDiaPorKg(0.5);
		conservado.setDiasConservacion(15);

		ProductoFabricado.setCostoHoraHombre(7.5);
		
	}
		
}
