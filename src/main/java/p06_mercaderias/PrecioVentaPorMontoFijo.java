package p06_mercaderias;

public class PrecioVentaPorMontoFijo implements CalculoPrecioVenta {

	private double montoFijo;
	
	// constructores
	
	public PrecioVentaPorMontoFijo(double montoFijo) {
		super();
		this.montoFijo = montoFijo;
	}

	// getters / setters

	public double getMontoFijo() {
		return montoFijo;
	}

	// consulta

	@Override
	public double getPrecioVenta(Producto producto) {
		return producto.getCosto() + this.montoFijo;
	}

}
