package p06_mercaderias;

import java.util.ArrayList;
import java.util.List;

public class ProductoFabricado extends Producto {

	private double cantHorasHombre;
	private static double costoHoraHombre;
	private List<Producto> componentes = new ArrayList<>();

	// constructores
	
	public ProductoFabricado(String nombre, double pesoProm, double valorAlmacPorKg, double cantHorasHombre) {
		super(nombre, pesoProm, valorAlmacPorKg);
		this.cantHorasHombre = cantHorasHombre;
	}

	// getters / setters

	public double getCantHorasHombre() {
		return this.cantHorasHombre;
	}
	
	public static double getCostoHoraHombre() {
		return ProductoFabricado.costoHoraHombre;
	}
	
	public static void setCostoHoraHombre(double costoHoraHombre) {
		ProductoFabricado.costoHoraHombre = costoHoraHombre;
	}
	
	// altas a colecciones
	
	public void agregarComponente(Producto componente) {
		this.componentes.add(componente);
	}
	
	// consultas
	
	@Override
	public double getCostoProduccion() {
		return this.cantHorasHombre * ProductoFabricado.costoHoraHombre;
	}

	@Override
	public double getCosto() {
		return super.getCosto() + this.costoDeComponentes();
	}

	private double costoDeComponentes() {
		return this.componentes.stream()
				.mapToDouble(componente -> componente.getCosto())
				.sum();
	}

}
