package p06_mercaderias;

public interface EstadoEstacional {
	// patrón State simplificado (sin clase contexto)

	double getPrecioVenta(Producto producto);

}
