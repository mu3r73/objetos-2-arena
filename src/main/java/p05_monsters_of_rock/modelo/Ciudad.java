package p05_monsters_of_rock.modelo;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * ciudad
 * (pertenece a un país, tiene sedes,
 * puede organizar eventos - a los que sólo pueden agregarse bandas del mismo país que la ciudad,
 * y su sede también debe estar en el mismo país que la ciudad)
 */
public class Ciudad implements Organizador {

	private String nombre;
	private Pais pais;
	Set<Sede> sedes = new HashSet<>();
	
	// constructores

	public Ciudad(String nombre, Pais pais) {
		super();
		this.nombre = nombre;
		this.pais = pais;
		this.pais.agregarCiudad(this);
	}
	
	// getters / setters
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Pais getPais() {
		return this.pais;
	}
	
	// altas a colecciones
	
	protected void agregarSede(Sede sede) {
		this.sedes.add(sede);
	}
	
	// consultas

	/**
	 * indica si puede organizar eventos en la sede 
	 */
	@Override
	public void checkPuedeTrabajarEn(Sede sede) {
		if (!this.estaEnElMismoPaisQue(sede.getCiudad())) {
			throw new RuntimeException(this.getNombre() + " no está en el mismo país que " + sede.getNombre());
		}
	}

	/**
	 * indica si la ciudad está en el mismo país que esta ciudad
	 */
	private boolean estaEnElMismoPaisQue(Ciudad ciudad) {
		return this.pais.equals(ciudad.getPais());
	}

	/**
	 * verifica si la banda puede participar en eventos organizados por la ciudad
	 */
	@Override
	public void checkPuedeTrabajarCon(Banda banda, Disco disco) {
		if (!this.pais.equals(banda.getPais())) {
			throw new RuntimeException(banda.getNombre() + " no es de " + this.pais.getNombre());
		}
	}
	
	/**
	 * retorna las bandas extranjeras que participan en eventos en el país
	 */
	protected Set<Banda> bandasExtranjerasQueParticipanEnEventos() {
		return this.sedes.stream()
				.flatMap(sede -> sede.bandasExtranjerasQueParticipanEnEventos().stream())
				.collect(Collectors.toSet());
	}

}
