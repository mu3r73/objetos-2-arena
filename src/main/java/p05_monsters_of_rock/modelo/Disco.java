package p05_monsters_of_rock.modelo;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.uqbar.commons.utils.Observable;

/**
 * disco musical
 * (pertenece a una banda, tiene un año de edición,
 * fue producido por una discográfica,
 * y conoce su cantidad de copias vendidas por país)
 */
@Observable
public class Disco {

	private String nombre;
	private Banda banda;
	private int anioDeEdicion;
	private Discografica discografica;
	private Set<CopiasPorPais> copiasPorPais = new HashSet<>();
	
	// constructores
	
	public Disco(String nombre, Banda banda, int anioDeProduccion, Discografica discografica) {
		super();
		this.nombre = nombre;
		this.banda = banda;
		this.banda.agregarDisco(this);
		this.anioDeEdicion = anioDeProduccion;
		this.discografica = discografica;
	}
	
	// getters / setters
	
	public String getNombre() {
		return this.nombre;
	}
	
	public Banda getBanda() {
		return this.banda;
	}
	
	public int getAnioDeEdicion() {
		return this.anioDeEdicion;
	}
	
	public Set<CopiasPorPais> getCopiasPorPais() {
		return this.copiasPorPais;
	}
	
	public void setCopiasPorPais(Set<CopiasPorPais> cpp) {
		this.copiasPorPais = cpp;
	}

	// altas y bajas de colecciones
	
	public void agregarCopiasVendidas(CopiasPorPais cpp) {
		this.agregarCopiasVendidas(cpp.getPais(), cpp.getCopias());
	}
	
	public void agregarCopiasVendidas(Pais pais, int copias) {
		Optional<CopiasPorPais> cpp = this.buscarCopiasEn(pais);
		if (cpp.isPresent()) {
			cpp.get().setCopias(cpp.get().getCopias() + copias);
		} else {
			this.copiasPorPais.add(new CopiasPorPais(pais, copias));
		}
	}
	
	public void borrarCopiasVendidas(CopiasPorPais cpp) {
		this.copiasPorPais.remove(cpp);
	}
	
	// consultas
	
	/**
	 * cuántas copias en total se vendieron del disco 
	 */
	public int getTotalCopiasVendidas() {
		return this.copiasPorPais.stream()
				.mapToInt(cpp -> cpp.getCopias())
				.sum();
	}

	/**
	 * indica si el disco fue producido por la discográfica
	 */
	protected boolean fueProducidoPor(Discografica discografica) {
		return this.discografica.equals(discografica);
	}

	/**
	 * indica la cantidad de copias vendidas en el país
	 */
	protected int copiasVendidasEn(Pais pais) {
		Optional<CopiasPorPais> cpp = this.buscarCopiasEn(pais);
		if (cpp.isPresent()) {
			return cpp.get().getCopias();
		} else {
			return 0;
		}
	}

	public Optional<CopiasPorPais> buscarCopiasEn(Pais pais) {
		return this.copiasPorPais.stream()
				.filter(cpp -> cpp.getPais().equals(pais))
				.findAny();
	}
	
	/**
	 * indica si el disco pertenece a la banda
	 */
	protected boolean esDeLaBanda(Banda banda) {
		return this.banda.equals(banda);
	}

}
