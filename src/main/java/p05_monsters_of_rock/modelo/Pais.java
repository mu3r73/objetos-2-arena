package p05_monsters_of_rock.modelo;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.uqbar.commons.utils.Observable;

/**
 * país
 * (tiene ciudades)
 */
@Observable
public class Pais {

	private String nombre;
	private Set<Ciudad> ciudades = new HashSet<>();
	
	// constructores
	
	public Pais(String nombre) {
		super();
		this.nombre = nombre;
	}
	
	// getters / setters

	public String getNombre() {
		return this.nombre;
	}
	
	public Set<Ciudad> getCiudades() {
		return this.ciudades;
	}
	
	public void setCiudades(Set<Ciudad> ciudad) {
		this.ciudades = ciudad;
	}
	
	// altas a colecciones
	
	protected void agregarCiudad(Ciudad ciudad) {
		this.ciudades.add(ciudad);
	}
	
	// consultas

	/**
	 * retorna las bandas extranjeras que participan en eventos en el país
	 */
	public Set<Banda> bandasExtranjerasQueParticipanEnEventos() {
		return this.ciudades.stream()
				.flatMap(ciudad -> ciudad.bandasExtranjerasQueParticipanEnEventos().stream())
				.collect(Collectors.toSet());
	}

}
