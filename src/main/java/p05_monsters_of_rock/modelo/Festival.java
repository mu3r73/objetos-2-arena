package p05_monsters_of_rock.modelo;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import org.uqbar.commons.utils.Observable;

/**
 * festival
 * (es un evento,
 * tiene géneros musicales permitidos, y auspiciantes,
 * no puede durar más de 12 horas,
 * se presenta 1 banda principal (de cierre), y N bandas soporte,
 * la banda principal debe tener más copias vendidas que las bandas soporte,
 * la sede debe ser un estadio)
 */
@Observable
public class Festival extends Evento {

	private Set<String> generosPermitidos = new HashSet<>();
	private Set<Auspiciante> auspiciantes = new HashSet<>();
	private static final int duracionMaxima = 12 * 60; // en minutos

	// constructores

	public Festival(String nombre, Organizador organizador, double precioEntrada, double financiamInicial) {
		super(nombre, organizador, precioEntrada, financiamInicial);
	}
	
	// getters / setters
	
	public Set<String> getGenerosPermitidos() {
		return this.generosPermitidos;
	}
	
	public void setGenerosPermitidos(Set<String> generos) {
		this.generosPermitidos = generos;
	}
	
	public Set<Auspiciante> getAuspiciantes() {
		return this.auspiciantes;
	}
	
	public void setAuspiciantes(Set<Auspiciante> auspiciantes) {
		this.auspiciantes = auspiciantes;
	}
	
	// altas a colecciones
	
	public void agregarGeneroPermitido(String genero) {
		generosPermitidos.add(genero);
	}
	
	public void agregarAporte(String nombre, double aporte) {
		auspiciantes.add(new Auspiciante(nombre, aporte));
	}	
	
	/**
	 * retorna el monto mínimo que se espera recaudar por el festival
	 */
	@Override
	public double getIngresoAsegurado() {
		return this.financiamInicial
				+ this.aporteDeAuspiciantes();
	}

	/**
	 * retorna el monto total aportado por auspiciantes
	 */
	private double aporteDeAuspiciantes() {
		return this.auspiciantes.stream()
				.mapToDouble(auspiciante -> auspiciante.getAporte())
				.sum();
	}

	/**
	 * retorna los gastos básicos del festival
	 */
	@Override
	public double getGastosBasicos() {
		if (this.tieneBandaPrincipal()) {
			return this.cachetBandaPrincipal()
					+ this.cachetBandasSoporte() / 2.0;
		} else {
			return 0;
		}
	}

	/**
	 * retorna la suma de los cachets de las bandas soporte
	 */
	private double cachetBandasSoporte() {
		return this.bandasSoporte().stream()
				.mapToDouble(banda -> banda.getCachet())
				.sum();
	}

	/**
	 * indica si la banda puede agregarse al festival,
	 * presentando el disco y tocando duración minutos
	 */ 
	@Override
	public void checkPuedeAgregarse(Banda banda, Disco disco, int duracion) {
		super.checkPuedeAgregarse(banda, disco, duracion);
		this.checkEsGeneroPermitido(banda);
		this.checkExcedeDuracionMaxima(duracion);
		this.checkNoEsCompatibleConPrincipal(banda);
	}

	private void checkEsGeneroPermitido(Banda banda) {
		if (!this.esGeneroPermitido(banda.getGenero())) {
			throw new RuntimeException("género de " + banda.getNombre() + " (" + banda.getGenero() + ") no permitido");
		}
	}

	private void checkExcedeDuracionMaxima(int duracion) {
		if (!this.noExcedeDuracionMax(duracion)) {
			throw new RuntimeException("duración: " + duracion + " min excede la duración máxima (tiempo disponible: "
					+ (duracionMaxima - this.getDuracionTotal()) + " min)");
		}
	}

	private void checkNoEsCompatibleConPrincipal(Banda banda) {
		if (!this.esCompatibleConPrincipal(banda)) {
			throw new RuntimeException("demasiadas copias vendidas (máximo permitido: "
					+ (this.bandaPrincipal().getTotalCopiasVendidas() - 1) + ")");
		}
	}

	/**
	 * indica si el género es importante en el festival
	 */
	@Override
	protected boolean esGeneroImportante(String genero) {
		return super.esGeneroImportante(genero)
				|| this.cantBandasSoporteConGenero(genero) >= 3;
	}

	/**
	 * retorna la cantidad de bandas soporte del género
	 */
	private int cantBandasSoporteConGenero(String genero) {
		return this.bandasSoporteConGenero(genero).size();
	}

	/**
	 * retorna las bandas soporte con el género indicado
	 */
	private Set<Banda> bandasSoporteConGenero(String genero) {
		return this.bandasSoporte().stream()
				.filter(banda -> banda.getGenero().equals(genero))
				.collect(Collectors.toSet());
	}

	/**
	 * indica si el género está permitido en el festival  
	 */
	private boolean esGeneroPermitido(String genero) {
		return this.generosPermitidos.contains(genero);
	}
	
	/**
	 * indica si, agregando tiempoATocar, no se excede la duracionMaxima del festival
	 */
	private boolean noExcedeDuracionMax(int tiempoATocar) {
		return this.getDuracionTotal() + tiempoATocar <= Festival.duracionMaxima;
	}

	/**
	 * indica si la banda es compatible como o con la banda principal
	 */
	private boolean esCompatibleConPrincipal(Banda banda) {
		return !this.tieneBandaPrincipal()
				|| banda.vendioMenosDe(this.bandaPrincipal().getTotalCopiasVendidas());
	}

	/**
	 * indica que un festival no puede realizarse en un anfiteatro
	 */
	@Override
	protected void checkPuedeRealizarseEnAnfiteatro() {
		throw new RuntimeException("festival no puede realizarse en anfiteatro");
	}

	/**
	 * indica que un festival puede realizarse en un estadio
	 */
	@Override
	protected void checkPuedeRealizarseEnEstadio() {
		// Festival puede realizarse en Estadio
	}

}
