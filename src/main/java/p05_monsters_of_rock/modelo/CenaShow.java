package p05_monsters_of_rock.modelo;

import org.uqbar.commons.utils.Observable;

/**
 * cena-show
 * (es un evento,
 * se presenta 1 banda,
 * la sede debe ser un anfiteatro)
 */
@Observable
public class CenaShow extends Evento {

	private final static double gastoBasicoFijo = 80000;

	// constructores
	
	public CenaShow(String nombre, Organizador organizador, double precioEntrada, double financiamInicial) {
		super(nombre, organizador, precioEntrada, financiamInicial);
	}

	/**
	 * retorna el monto mínimo que se espera recaudar por la cena-show 
	 */
	@Override
	public double getIngresoAsegurado() {
		return this.financiamInicial + this.ingresoMaxPorEntradas() / 2;
	}

	/**
	 * retorna el monto máximo que se puede obtener por venta de entradas 
	 */
	private double ingresoMaxPorEntradas() {
		return this.sede.getCapacidad() * this.precioEntrada;
	}

	/**
	 * retorna los gastos básicos de la cena-show
	 */
	@Override
	public double getGastosBasicos() {
		return this.gastoBandaPrincipal() * 0.8
				+ CenaShow.gastoBasicoFijo;
	}

	/**
	 * si hay banda principal, retorna su cachet
	 * si no, retorna 0
	 */
	private double gastoBandaPrincipal() {
		if (this.tieneBandaPrincipal()) {
			return this.cachetBandaPrincipal();
		} else {
			return 0;
		}
	}

	/**
	 * verifica si la banda puede agregarse a la cena-show,
	 * presentando el disco y tocando duración minutos
	 */ 
	@Override
	public void checkPuedeAgregarse(Banda banda, Disco disco, int duracion) {
		super.checkPuedeAgregarse(banda, disco, duracion);
		this.checkYaTieneBandaPrincipal();
	}

	private void checkYaTieneBandaPrincipal() {
		if (this.tieneBandaPrincipal()) {
			throw new RuntimeException(this.getNombre() + " ya tiene banda principal");
		}
	}

	/**
	 * indica que la cena-show puede realizarse en un anfiteatro
	 */
	@Override
	protected void checkPuedeRealizarseEnAnfiteatro() {
		// Cena-Show puede realizarse en anfiteatro
	}

	/**
	 * indica que la cena-show no puede realizarse en un estadio
	 */
	@Override
	protected void checkPuedeRealizarseEnEstadio() {
		throw new RuntimeException(this.getNombre() + " es una cena-show => no puede realizarse en un estadio");
	}

}
