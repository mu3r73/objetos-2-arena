package p05_monsters_of_rock.modelo;

import java.util.HashSet;
import java.util.Set;

import org.uqbar.commons.utils.Observable;

/**
 * anfiteatro
 * (es una sede,
 * tiene un costo por evento, y géneros en promoción) 
 */
@Observable
public class Anfiteatro extends Sede {

	private double costoPorEvento;
	private Set<String> generosEnPromocion = new HashSet<>();

	// constructores
	
	public Anfiteatro(String nombre, Ciudad ciudad, int capacidad) {
		super(nombre, ciudad, capacidad);
	}

	// getters / setters
	
	public void setCostoPorEvento(double costoPorEvento) {
		this.costoPorEvento = costoPorEvento;
	}

	// altas a colecciones
	
	public void agregarGeneroEnPromocion(String genero) {
		this.generosEnPromocion.add(genero);
	}
	
	// consultas
	
	/**
	 * indica si el evento puede realizarse en el anfiteatro
	 */
	@Override
	protected void checkPuedeRecibir(Evento evento) {
		evento.checkPuedeRealizarseEnAnfiteatro();
	}

	/**
	 * retorna el costo del alquiler del anfiteatro, para el evento
	 */
	@Override
	protected double costoAlquiler(Evento evento) {
		if (this.tieneAlgunGeneroEnPromocionPara(evento)) {
			return this.costoPorEvento * 0.8;
		} else {
			return this.costoPorEvento;
		}
	}

	/**
	 * indica si el evento incluye algún género en promoción
	 */
	private boolean tieneAlgunGeneroEnPromocionPara(Evento evento) {
		return this.generosEnPromocion.stream()
				.anyMatch(genero -> evento.incluyeGenero(genero));
	}
	
}
