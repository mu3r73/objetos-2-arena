package p05_monsters_of_rock.modelo;

import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.uqbar.commons.utils.Observable;

/**
 * banda musical
 * (pertenece a un género musical, es de un país, tiene discos,
 * puede presentarse en eventos, cobra un cachet fijo por presentación)
 */
@Observable
public class Banda {

	private String nombre;
	private Set<Disco> discos = new HashSet<>();
	private String genero;
	private double cachet;
	private Pais pais;

	// constructores

	public Banda(String nombre, String genero, double cachet, Pais pais) {
		super();
		this.nombre = nombre;
		this.genero = genero;
		this.cachet = cachet;
		this.pais = pais;
	}

	// getters / setters

	public String getNombre() {
		return this.nombre;
	}
	
	public Set<Disco> getDiscos() {
		return this.discos;
	}
	
	public void setDiscos(Set<Disco> discos) {
		this.discos = discos;
	}

	public String getGenero() {
		return this.genero;
	}
	
	public double getCachet() {
		return this.cachet;
	}
	
	public void setCachet(double cachet) {
		this.cachet = cachet;
	}
	
	public Pais getPais() {
		return this.pais;
	}
	
	// altas a colecciones

	protected void agregarDisco(Disco disco) {
		this.discos.add(disco);
	}

	// consultas

	/**
	 * req 1:
	 * retorna la cantidad total de copias vendidas por la banda
	 */
	public int getTotalCopiasVendidas() {
		return this.discos.stream()
				.mapToInt(disco -> disco.getTotalCopiasVendidas())
				.sum();
	}

	/**
	 * req 2:
	 * retorna el último disco editado por la banda
	 */
	public Disco ultimoDisco() {
		return this.discos.stream()
				.max(Comparator.comparing(disco -> disco.getAnioDeEdicion()))
				.get();
	}

	/**
	 * req 4:
	 * indica si la banda puede agregarse al evento, con el disco y duración propuestos
	 */
	public boolean puedeAgregarseA(Evento evento, Disco disco, int duracion) {
		return evento.puedeAgregarse(this, disco, duracion);
	}
	
	public void checkPuedeAgregarseA(Evento evento, Disco disco, int duracion) {
		evento.checkPuedeAgregarse(this, disco, duracion);
	}
	
	/**
	 * indica si la banda vendio menos copias que el número indicado en cantidad
	 */
	protected boolean vendioMenosDe(int cantidad) {
		return this.getTotalCopiasVendidas() < cantidad;
	}

	/**
	 * indica si es del mismo género que la otra banda
	 */
	protected boolean esMismoGeneroQue(Banda banda) {
		return this.genero.equals(banda.getGenero());
	}

	/**
	 * retorna la cantidad total de copias vendidas en el país
	 */
	protected int cuantasCopiasVendioEn(Pais pais) {
		return this.discos.stream()
				.mapToInt(disco -> disco.copiasVendidasEn(pais))
				.sum();
	}
	
	public List<Disco> getDiscosOrdenAnio() {
		return this.discos.stream()
				.sorted(Comparator.comparing(disco -> disco.getAnioDeEdicion()))
				.collect(Collectors.toList());
	}

	// acciones
	
	/**
	 * req 5:
	 * agrega la banda al evento,
	 * presentando el disco y tocando duración minutos
	 * (el constructor de Presentacion valida que el disco pertenece a la banda)
	 */ 
	public void agregarseAEvento(Evento evento, Disco disco, int duracion) {
		evento.checkPuedeAgregarse(this, disco, duracion);
		evento.agregarPresentacion(this, disco, duracion);
	}
	
}
