package p05_monsters_of_rock.modelo;

import org.uqbar.commons.utils.Observable;

/**
 * recital
 * (es un evento,
 * tiene un importe para publicidad,
 * se presenta 1 banda principal (de cierre), y hasta 3 bandas soporte, del mismo género que la principal, 
 * la banda principal debe haber vendido al menos el triple de copias que cada banda soporte)
 */
@Observable
public class Recital extends Evento {

	private static final double ingresoFijo = 250000;
	private double importeParaPublicidad = 0;

	// constructores

	public Recital(String nombre, Organizador organizador, double precioEntrada, double financiamInicial) {
		super(nombre, organizador, precioEntrada, financiamInicial);
	}

	// getters / setters

	public void setImporteParaPublicidad(double importeParaPublicidad) {
		this.importeParaPublicidad = importeParaPublicidad;
	}

	/**
	 * retorna el monto mínimo que se espera recaudar por el recital
	 */
	@Override
	public double getIngresoAsegurado() {
		return this.financiamInicial + Recital.ingresoFijo;
	}

	/**
	 * retorna los gastos básicos del recital
	 */
	@Override
	public double getGastosBasicos() {
		return this.gastoBandaPrincipal()
				+ (this.sede.costoAlquiler(this) / 2)
				+ this.importeParaPublicidad;
	}
	
	/**
	 * si hay banda principal, retorna su cachet
	 * si no, retorna 0
	 */
	private double gastoBandaPrincipal() {
		if (this.tieneBandaPrincipal()) {
			return this.cachetBandaPrincipal();
		} else {
			return 0;
		}
	}

	/**
	 * indica si la banda puede agregarse al recital, presentando el disco y
	 * tocando duración minutos
	 */
	@Override
	public void checkPuedeAgregarse(Banda banda, Disco disco, int duracion) {
		super.checkPuedeAgregarse(banda, disco, duracion);
		if (this.tieneBandaPrincipal()) {
			this.checkDemasiadasBandasSoporte();
			this.checkEsGeneroPermitido(banda);
			this.checkVendioDemasiadasCopias(banda);
		}
	}

	private void checkDemasiadasBandasSoporte() {
		if (!hayMenosDeNBandasSoporte(3)) {
			throw new RuntimeException("ya se agregaron una banda principal y 3 bandas soporte");
		}
	}

	private void checkEsGeneroPermitido(Banda banda) {
		if (!banda.esMismoGeneroQue(this.bandaPrincipal())) {
			throw new RuntimeException("género de " + banda.getNombre() + " (" + banda.getGenero()
					+ ") incompatible con banda principal " + this.bandaPrincipal().getNombre()
					+ " (" + this.bandaPrincipal().getGenero() + ")");
		}
	}

	private void checkVendioDemasiadasCopias(Banda banda) {
		if (!banda.vendioMenosDe(this.bandaPrincipal().getTotalCopiasVendidas() / 3)) {
			throw new RuntimeException("demasiadas copias vendidas - máximo permitido: "
				+ ((this.bandaPrincipal().getTotalCopiasVendidas() / 3) - 1));
		}
	}

	/**
	 * indica si la cantidad de bandas de soporte es menor a cant
	 */
	private boolean hayMenosDeNBandasSoporte(int cant) {
		return this.bandasSoporte().size() < cant;
	}

	/**
	 * indica que el recital puede realizarse en un anfiteatro
	 */
	@Override
	protected void checkPuedeRealizarseEnAnfiteatro() {
		// Recital puede realizarse en Anfiteatro
	}

	/**
	 * indica que el recital puede realizarse en un estadio
	 */
	@Override
	protected void checkPuedeRealizarseEnEstadio() {
		// Recital puede realizarse en Estadio
	}

}
