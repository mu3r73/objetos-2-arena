package p05_monsters_of_rock.modelo;

/**
 * compañía discográfica
 * (puede producir discos,
 * puede organizar eventos - a los que sólo pueden agregarse bandas
 * a las que les produjo su último disco o el disco que presentan en el evento) 
 */
public class Discografica implements Organizador {

	private String nombre;
	
	// constructores
	
	public Discografica(String nombre) {
		super();
		this.nombre = nombre;
	}

	// getters / setters
	
	public String getNombre() {
		return this.nombre;
	}
	
	// consultas
	
	/**
	 * indica que la discográfica puede organizar eventos en cualquier sede 
	 */
	@Override
	public void checkPuedeTrabajarEn(Sede sede) {
		// puede trabajar en cualquier sede
	}

	/**
	 * verifica si la banda puede participar en eventos organizados por la discográfica
	 */
	@Override
	public void checkPuedeTrabajarCon(Banda banda, Disco disco) {
		if (!this.produjoUltimoDiscoDe(banda)
				&& !disco.fueProducidoPor(this)) {
			throw new RuntimeException(this.nombre + " no produjo el último disco de "
					+ banda.getNombre() + " - disco presentado " + disco.getNombre()
					+ " tampoco fue producido por " + this.nombre);
		}
	}

	/**
	 * indica si la discográfica produjo el último disco de la banda
	 */
	private boolean produjoUltimoDiscoDe(Banda banda) {
		return banda.ultimoDisco().fueProducidoPor(this);
	}

}
