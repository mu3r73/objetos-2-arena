package p05_monsters_of_rock.modelo;

import org.uqbar.commons.utils.Observable;

/**
 * presentación de una banda en un evento
 * (tiene una banda, un disco perteneciente a esa banda,
 * y una duración en minutos)
 */
@Observable
public class Presentacion {

	private Banda banda;
	private Disco disco;
	private int duracion; // en minutos

	// constructores
	
	public Presentacion() {
		super();
	}
	
	public Presentacion(Banda banda, Disco disco, int duracion) {
		super();
		this.banda = banda;
		this.disco = disco;
		this.duracion = duracion;		
	}

	// getters / setters
	
	public Banda getBanda() {
		return this.banda;
	}
	
	public void setBanda(Banda banda) {
		this.banda = banda;
	}
	
	public Disco getDisco() {
		return this.disco;
	}
	
	public void setDisco(Disco disco) {
		this.disco = disco;
	}
	
	public int getDuracion() {
		return this.duracion;
	}
	
	public void setDuracion(int duracion) {
		this.duracion = duracion;
	}
	
}
