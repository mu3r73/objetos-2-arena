package p05_monsters_of_rock.modelo;

import org.uqbar.commons.utils.Observable;

/**
 * estadio
 * (es una sede,
 * tiene un costo de alquiler fijo por hora)
 */
@Observable
public class Estadio extends Sede {

	private double costoAlquilerPorHora;

	// constructores
	
	public Estadio(String nombre, Ciudad ciudad, int capacidad) {
		super(nombre, ciudad, capacidad);
	}
	
	// getters / setters
	
	public void setCostoAlquilerPorHora(double costoAlquilerPorHora) {
		this.costoAlquilerPorHora = costoAlquilerPorHora;
	}

	// consultas
	
	/**
	 * verifica si el evento puede realizarse en el estadio
	 */
	@Override
	protected void checkPuedeRecibir(Evento evento) {
		evento.checkPuedeRealizarseEnEstadio();
	}

	/**
	 * retorna el costo del alquiler del estadio, para el evento
	 */
	@Override
	protected double costoAlquiler(Evento evento) {
		return (evento.getDuracionTotal() / 60.0) * this.costoAlquilerPorHora;
	}

}
