package p05_monsters_of_rock.modelo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.uqbar.commons.utils.Observable;

/**
 * evento genérico
 * (tiene un organizador, un precio único de entrada,
 * un monto de financiamiento inicial,
 * una sede - que debería asignarse inmediatamente después de crear el evento,
 * y una serie de presentaciones - que deben agregarse después de asignar una sede)
 */
@Observable
public abstract class Evento {

	private String nombre;
	private Organizador organizador;
	protected double precioEntrada;
	protected double financiamInicial;
	protected Sede sede;
	private List<Presentacion> presentaciones = new ArrayList<>();

	// constructores

	public Evento(String nombre, Organizador organizador, double precioEntrada, double financiamInicial) {
		this.nombre = nombre;
		this.organizador = organizador;
		this.precioEntrada = precioEntrada;
		this.financiamInicial = financiamInicial;
	}

	// getters / setters

	public String getNombre() {
		return this.nombre;
	}
	
	public void setPrecioEntrada(Double precio) {
		this.precioEntrada = precio;
	}
	
	public Sede getSede() {
		return this.sede;
	}

	public List<Presentacion> getPresentaciones() {
		return this.presentaciones;
	}
	
	public void setPresentaciones(List<Presentacion> presentaciones) {
		this.presentaciones = presentaciones;
	}
	
	public Organizador getOrganizador() {
		return this.organizador;
	}

	// consultas

	/**
	 * indica si la banda puede agregarse al evento, presentando el disco y
	 * tocando duración minutos
	 */
	protected boolean puedeAgregarse(Banda banda, Disco disco, int duracion) {
		try {
			this.checkPuedeAgregarse(banda, disco, duracion);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	public void checkPuedeAgregarse(Banda banda, Disco disco, int duracion) {
		this.checkNoTieneSede();
		this.checkYaFueAgregada(banda);
		this.checkDiscoEsDeLaBanda(banda, disco);
		this.organizador.checkPuedeTrabajarCon(banda, disco);
	}

	private void checkNoTieneSede() {
		if (!this.tieneSede()) {
			throw new RuntimeException("evento " + nombre + " no tiene sede");
		}
	}
	
	private void checkYaFueAgregada(Banda banda) {
		if (this.yaFueAgregada(banda)) {
			throw new RuntimeException(banda.getNombre() + " ya se agregó al evento");
		}
	}

	private void checkDiscoEsDeLaBanda(Banda banda, Disco disco) {
		if (!disco.esDeLaBanda(banda)) {
			throw new RuntimeException(disco.getNombre() + " no pertenece a banda " + banda.getNombre());
		}
	}

	/**
	 * indica si la banda ya se agregó al evento
	 */
	private boolean yaFueAgregada(Banda banda) {
		return this.tieneBandaPrincipal() && this.todasLasBandas().contains(banda);
	}

	/**
	 * indica si el evento tiene asignada una sede
	 */
	public boolean tieneSede() {
		return this.sede != null;
	}

	/**
	 * req 9:
	 * indica si el evento puede realizarse en la sede
	 */
	public boolean puedeRealizarseEn(Sede sede) {
		try {
			this.checkPuedeRealizarseEn(sede);
			return true;
		} catch (Exception e) {
			return false;
		}
	}

	private void checkPuedeRealizarseEn(Sede sede) {
		organizador.checkPuedeTrabajarEn(sede);
		sede.checkPuedeRecibir(this);
	}

	/**
	 * req 6:
	 * indica si el evento incluye el género
	 */
	public boolean incluyeGenero(String genero) {
		this.checkTieneBandaPrincipal();
		return this.esGeneroImportante(genero);
	}

	private void checkTieneBandaPrincipal() {
		if (!this.tieneBandaPrincipal()) {
			throw new RuntimeException("no hay banda principal");
		}
	}

	/**
	 * req 7:
	 * retorna el costo del alquiler de la sede, para el evento
	 */
	public double costoAlquilerDeSede() {
		return this.sede.costoAlquiler(this);
	}

	/**
	 * req 8:
	 * indica si el evento es económicamente factible
	 */
	public boolean esEconomicamenteFactible() {
		return this.getIngresoAsegurado() > this.getGastosBasicos();
	}

	/**
	 * req 10:
	 * retorna el índice de éxito potencial del evento
	 */
	public double indiceDeExitoPotencial() {
		return this.totalDeCopiasVendidasEnElPais()
				* (1 + 0.05 * this.cantBandasDelPais(this.pais()));
	}

	/**
	 * retorna la cantidad total de copias vendidas por las bandas que
	 * participan en el evento en el país del evento
	 */
	private int totalDeCopiasVendidasEnElPais() {
		return this.todasLasBandas().stream()
				.mapToInt(banda -> banda.cuantasCopiasVendioEn(this.pais()))
				.sum();
	}

	/**
	 * retorna la cantidad de bandas que participan en el evento que son
	 * originarias del mismo país del evento
	 */
	private int cantBandasDelPais(Pais pais) {
		return this.bandasDelPais(pais).size();
	}

	/**
	 * retorna las bandas que participan en el evento que son originarias del
	 * mismo país del evento
	 */
	private Set<Banda> bandasDelPais(Pais pais) {
		return todasLasBandas().stream()
				.filter(banda -> banda.getPais().equals(this.pais()))
				.collect(Collectors.toSet());
	}

	/**
	 * retorna el país del evento
	 */
	private Pais pais() {
		return this.sede.getPais();
	}

	/**
	 * indica si el género es importante en el evento
	 */
	protected boolean esGeneroImportante(String genero) {
		return this.esGeneroDeBandaPrincipal(genero);
	}

	/**
	 * indica si es el género de la banda principal
	 */
	private boolean esGeneroDeBandaPrincipal(String genero) {
		return this.bandaPrincipal().getGenero().equals(genero);
	}

	/**
	 * retorna el monto mínimo que se espera recaudar por el evento
	 */
	public abstract double getIngresoAsegurado();

	/**
	 * retorna los gastos básicos del evento
	 */
	public abstract double getGastosBasicos();

	/**
	 * retorna el cachet de la banda principal
	 */
	protected double cachetBandaPrincipal() {
		return this.bandaPrincipal().getCachet();
	}

	/**
	 * retorna la lista de todas las bandas que participan en el evento
	 */
	public Set<Banda> todasLasBandas() {
		Set<Banda> bandas = new HashSet<>();
		bandas.add(this.bandaPrincipal());
		bandas.addAll(this.bandasSoporte());
		return bandas;
	}

	/**
	 * retorna la duración total del evento
	 */
	public int getDuracionTotal() {
		if (this.tieneBandaPrincipal()) {
			return this.presentPrincipal().getDuracion() + this.duracionSoporte();
		} else {
			return 0;
		}
	}

	/**
	 * retorna la duración total de las presentaciones de las bandas soporte
	 */
	private int duracionSoporte() {
		return this.presentSoporte().stream()
				.mapToInt(presentacion -> presentacion.getDuracion())
				.sum();
	}

	/**
	 * indica si el evento ya tiene banda principal
	 */
	protected boolean tieneBandaPrincipal() {
		return this.presentPrincipal() != null;
	}

	/**
	 * retorna la banda principal del evento
	 */
	protected Banda bandaPrincipal() {
		return this.presentPrincipal().getBanda();
	}
	
	/**
	 * indica si es la banda principal del evento 
	 */
	public boolean esBandaPrincipal(Banda banda) {
		return this.tieneBandaPrincipal() && this.bandaPrincipal().equals(banda);
	}
	
	/**
	 * indica si el evento tiene bandas soporte
	 */
	public boolean tieneBandasSoporte() {
		return this.tieneBandaPrincipal() && !this.bandasSoporte().isEmpty();
	}
	
	/**
	 * retorna las bandas de soporte del evento
	 */
	protected Set<Banda> bandasSoporte() {
		return this.presentSoporte().stream()
				.map(presentacion -> presentacion.getBanda())
				.collect(Collectors.toSet());
	}

	/**
	 * retorna la presentación principal - head(presentacioens)
	 */
	private Presentacion presentPrincipal() {
		if (presentaciones.isEmpty()) {
			return null;
		} else {
			return presentaciones.get(0);
		}
	}

	/**
	 * retorna las presentaciones soporte - tail(presentaciones)
	 */
	private List<Presentacion> presentSoporte() {
		return presentaciones.subList(1, presentaciones.size());
	}

	/**
	 * indica si el evento puede realizarse en un anfiteatro
	 */
	protected abstract void checkPuedeRealizarseEnAnfiteatro();

	/**
	 * indica si el evento puede realizarse en un estadio
	 */
	protected abstract void checkPuedeRealizarseEnEstadio();

	// acciones

	/**
	 * agrega la presentación de una banda a un evento
	 * (el constructor de Presentación valida que el disco pertenece a la banda)
	 */
	protected void agregarPresentacion(Banda banda, Disco disco, int duracion) {
		this.presentaciones.add(new Presentacion(banda, disco, duracion));
	}
	
	/**
	 * borra una presentación
	 */
	public void borrarPresentacion(Presentacion presentacion) {
		this.presentaciones.remove(presentacion);
	}

	/**
	 * agrega un evento a una sede
	 */
	public void agregarSede(Sede sede) {
		this.checkTieneSede();
		this.checkPuedeRealizarseEn(sede);
		this.sede = sede;
		sede.agregarEvento(this);
	}

	private void checkTieneSede() {
		if (this.tieneSede()) {
			throw new RuntimeException("evento ya tiene sede, no se puede cambiar");
		}
	}

}
