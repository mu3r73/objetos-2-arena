package p05_monsters_of_rock.stores;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.uqbar.commons.utils.Observable;

import p05_monsters_of_rock.modelo.Pais;

@Observable
public class PaisesStore {

	private static PaisesStore instancia;
	private Collection<Pais> paises;
	
	// constructores
	
	private PaisesStore() {
		super();
	}
	
	// instancia singleton
	
	public static PaisesStore getInstancia() {
		if (instancia == null) {
			instancia = new PaisesStore();
		}
		return instancia;
	}
	
	// datos
	
	public void cargarDatosPrueba() {
		if (this.paises ==  null) {
			Pais alemania = new Pais("Alemania");
			Pais arabiaS = new Pais("Arabia Saudita");
			Pais argentina = new Pais("Argentina");
			Pais australia = new Pais("Australia");
			Pais austria = new Pais("Austria");
			Pais brasil = new Pais("Brasil");
			Pais canada = new Pais("Canadá");
			Pais catar = new Pais("Catar");
			Pais china = new Pais("China");
			Pais coreaS = new Pais("Corea del Sur");
			Pais costaRica = new Pais("Costa Rica");
			Pais croacia = new Pais("Croacia");
			Pais dinamarca = new Pais("Dinamarca");
			Pais eeuu = new Pais("EEUU");
			Pais egipto = new Pais("Egipto");
			Pais emiratos = new Pais("Emiratos Árabes");
			Pais espania = new Pais("España");
			Pais filipinas = new Pais("Filipinas");
			Pais finlandia = new Pais("Finlandia");
			Pais francia = new Pais("Francia");
			Pais grecia = new Pais("Grecia");
			Pais hungria = new Pais("Hungría");
			Pais india = new Pais("India");
			Pais indonesia = new Pais("Indonesia");
			Pais inglaterra = new Pais("Inglaterra");
			Pais irlanda = new Pais("Irlanda");
			Pais israel = new Pais("Israel");
			Pais italia = new Pais("Italia");
			Pais japon = new Pais("Japón");
			Pais luxemburgo = new Pais("Luxemburgo");
			Pais malasia = new Pais("Malasia");
			Pais marruecos = new Pais("Marruecos");
			Pais mexico = new Pais("México");
			Pais noruega = new Pais("Noruega");
			Pais nz = new Pais("Nueva Zelandia");
			Pais paisesBajos = new Pais("Países Bajos");
			Pais peru = new Pais("Perú");
			Pais polonia = new Pais("Polonia");
			Pais panama = new Pais("Panamá");
			Pais portugal = new Pais("Portugal");
			Pais repCheca = new Pais("República Checa");
			Pais rusia = new Pais("Rusia");
			Pais singapur = new Pais("Singapur");
			Pais sriLanka = new Pais("Sri Lanka");
			Pais sudafrica = new Pais("Sudáfrica");
			Pais suecia = new Pais("Suecia");
			Pais suiza = new Pais("Suiza");
			Pais tailandia = new Pais("Tailandia");
			Pais turquia = new Pais("Turquia");
			Pais vietnam = new Pais("Vietnam");
	
			this.paises = new HashSet<>(Arrays.asList(alemania, arabiaS, argentina, australia, austria,
					brasil, canada, catar, china, coreaS, costaRica, croacia, dinamarca, eeuu, egipto,
					emiratos, espania, filipinas, finlandia, francia, grecia, hungria, india, indonesia,
					inglaterra, irlanda, israel, italia, japon, luxemburgo, malasia, marruecos, mexico,
					noruega, nz, paisesBajos, panama, peru, polonia, portugal, repCheca, rusia, singapur,
					sriLanka, sudafrica, suecia, suiza, tailandia, turquia, vietnam));
		}
	}
	
	// consultas
	
	public List<Pais> getPaisesOrdenNombre() {
		return this.paises.stream()
				.sorted(Comparator.comparing(pais -> pais.getNombre()))
				.collect(Collectors.toList());
	}
	
	public Pais getPais(String nombre) {
		return this.paises.stream()
				.filter(pais -> pais.getNombre().equals(nombre))
				.findAny()
				.get();
	}
	
}
