package p05_monsters_of_rock.stores;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashSet;

import org.uqbar.commons.utils.Observable;

import p05_monsters_of_rock.modelo.Disco;
import p05_monsters_of_rock.ui.DiscoConImagenTapa;

@Observable
public class DiscosStore {

	private static DiscosStore instancia;
	private Collection<DiscoConImagenTapa> discos;
	
	// constructores
	
	private DiscosStore() {
		super();
	}
	
	// instancia singleton
	
	public static DiscosStore getInstancia() {
		if (instancia == null) {
			instancia = new DiscosStore();
		}
		return instancia;
	}
	
	// datos
	
	public void cargarDatosPrueba() {
		if (this.discos == null) {
			PaisesStore ps = PaisesStore.getInstancia();
			ps.cargarDatosPrueba();
			
			DiscograficasStore ds = DiscograficasStore.getInstancia();
			ds.cargarDatosPrueba();
			
			BandasStore bs = BandasStore.getInstancia();
			bs.cargarDatosPrueba();
			
			DiscoConImagenTapa concealed = new DiscoConImagenTapa("Concealed", bs.getBanda("Augury"), 2004, ds.getDiscografica("Season of Mist"));
			DiscoConImagenTapa fragmentaryEvidence = new DiscoConImagenTapa("Fragmentary Evidence", bs.getBanda("Augury"), 2009, ds.getDiscografica("Season of Mist"));
			
			DiscoConImagenTapa leprosy = new DiscoConImagenTapa("Leprosy", bs.getBanda("Death"), 1988, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa spiritualHealing = new DiscoConImagenTapa("Spiritual Healing", bs.getBanda("Death"), 1990, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa human = new DiscoConImagenTapa("Human", bs.getBanda("Death"), 1991, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa symbolic = new DiscoConImagenTapa("Symbolic", bs.getBanda("Death"), 1995, ds.getDiscografica("Nuclear Blast"));
			
			DiscoConImagenTapa consideredDead = new DiscoConImagenTapa("Considered Dead", bs.getBanda("Gorguts"), 1991, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa erosionOfSanity = new DiscoConImagenTapa("The Erosion of Sanity", bs.getBanda("Gorguts"), 1993, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa obscura = new DiscoConImagenTapa("Obscura", bs.getBanda("Gorguts"), 1998, ds.getDiscografica("Olympic Recordings"));
			DiscoConImagenTapa wisdom2hate = new DiscoConImagenTapa("From Wisdom to Hate", bs.getBanda("Gorguts"), 2001, ds.getDiscografica("Olympic Recordings"));
			DiscoConImagenTapa coloredSands = new DiscoConImagenTapa("Colored Sands", bs.getBanda("Gorguts"), 2013, ds.getDiscografica("Season of Mist"));
			
			DiscoConImagenTapa syzygialMiscreancy = new DiscoConImagenTapa("Syzygial Miscreancy", bs.getBanda("Hellwitch"), 1990, ds.getDiscografica("Season of Mist"));
			DiscoConImagenTapa omnipotentConvocation = new DiscoConImagenTapa("Omnipotent Convocation", bs.getBanda("Hellwitch"), 2009, ds.getDiscografica("Season of Mist"));
			
			DiscoConImagenTapa hopelessHopes = new DiscoConImagenTapa("Hopeless Hopes", bs.getBanda("Martyr"), 1997, ds.getDiscografica("Season of Mist"));
			DiscoConImagenTapa warpZone = new DiscoConImagenTapa("Warp Zone", bs.getBanda("Martyr"), 2000, ds.getDiscografica("Season of Mist"));
			DiscoConImagenTapa feedingTheAbscess = new DiscoConImagenTapa("Feeding the Abscess", bs.getBanda("Martyr"), 2006, ds.getDiscografica("Season of Mist"));
			
			DiscoConImagenTapa paleDebilitatingAutumn = new DiscoConImagenTapa("A Pale Debilitating Autumn", bs.getBanda("Pavor"), 1994, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa furioso = new DiscoConImagenTapa("Furioso", bs.getBanda("Pavor"), 2003, ds.getDiscografica("Nuclear Blast"));
			
			DiscoConImagenTapa arise = new DiscoConImagenTapa("Arise", bs.getBanda("Sepultura"), 1991, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa chaosAD = new DiscoConImagenTapa("Chaos A.D.", bs.getBanda("Sepultura"), 1993, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa roots = new DiscoConImagenTapa("Roots", bs.getBanda("Sepultura"), 1996, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa against = new DiscoConImagenTapa("Against", bs.getBanda("Sepultura"), 1998, ds.getDiscografica("Season of Mist"));
			
			DiscoConImagenTapa hellAwaits = new DiscoConImagenTapa("Hell Awaits", bs.getBanda("Slayer"), 1985, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa reignInBlood = new DiscoConImagenTapa("Reign in Blood", bs.getBanda("Slayer"), 1986, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa southOfHeaven = new DiscoConImagenTapa("South of Heaven", bs.getBanda("Slayer"), 1988, ds.getDiscografica("Nuclear Blast"));
			
			DiscoConImagenTapa redAlbum = new DiscoConImagenTapa("Red Album", bs.getBanda("Baroness"), 2007, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa blueRecord = new DiscoConImagenTapa("Blue Record", bs.getBanda("Baroness"), 2009, ds.getDiscografica("Nuclear Blast"));
			
			DiscoConImagenTapa focus = new DiscoConImagenTapa("Focus", bs.getBanda("Cynic"), 1993, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa tracedInAir = new DiscoConImagenTapa("Traced in Air", bs.getBanda("Cynic"), 2008, ds.getDiscografica("Season of Mist"));
			DiscoConImagenTapa kindlyBent = new DiscoConImagenTapa("Kindly Bent to Free Us", bs.getBanda("Cynic"), 2014, ds.getDiscografica("Nuclear Blast"));
			
			DiscoConImagenTapa imagesNWords = new DiscoConImagenTapa("Images and Words", bs.getBanda("Dream Theater"), 1992, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa awake = new DiscoConImagenTapa("Awake", bs.getBanda("Dream Theater"), 1994, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa fallingIntoInf = new DiscoConImagenTapa("Falling into Infinity", bs.getBanda("Dream Theater"), 1997, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa metroPt2 = new DiscoConImagenTapa("Metropolis Pt. 2: Scenes from a Memory", bs.getBanda("Dream Theater"), 1999, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa sixDegrees = new DiscoConImagenTapa("Six Degrees of Inner Turbulence", bs.getBanda("Dream Theater"), 2002, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa trainOfThought = new DiscoConImagenTapa("Train of Thought", bs.getBanda("Dream Theater"), 2003, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa octavarium = new DiscoConImagenTapa("Octavarium", bs.getBanda("Dream Theater"), 2005, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa systematicChaos = new DiscoConImagenTapa("Systematic Chaos", bs.getBanda("Dream Theater"), 2007, ds.getDiscografica("Nuclear Blast"));
			
			DiscoConImagenTapa remission = new DiscoConImagenTapa("Remission", bs.getBanda("Mastodon"), 2002, ds.getDiscografica("Season of Mist"));
			DiscoConImagenTapa leviathan = new DiscoConImagenTapa("Leviathan", bs.getBanda("Mastodon"), 2004, ds.getDiscografica("Season of Mist"));
			DiscoConImagenTapa bloodMountain = new DiscoConImagenTapa("Blood Mountain", bs.getBanda("Mastodon"), 2006, ds.getDiscografica("Season of Mist"));
			DiscoConImagenTapa crackTheSkye = new DiscoConImagenTapa("Crack the Skye", bs.getBanda("Mastodon"), 2009, ds.getDiscografica("Season of Mist"));
			DiscoConImagenTapa hunter = new DiscoConImagenTapa("The Hunter", bs.getBanda("Mastodon"), 2011, ds.getDiscografica("Season of Mist"));
			
			DiscoConImagenTapa damnationGame = new DiscoConImagenTapa("The Damnation Game", bs.getBanda("Symphony X"), 1995, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa divineWings = new DiscoConImagenTapa("The Divine Wings of Tragedy", bs.getBanda("Symphony X"), 1997, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa twilightInOlympus = new DiscoConImagenTapa("Twilight in Olympus", bs.getBanda("Symphony X"), 1998, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa newMythologySuite = new DiscoConImagenTapa("V: The New Mythology Suite", bs.getBanda("Symphony X"), 2000, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa oddysey = new DiscoConImagenTapa("The Oddysey", bs.getBanda("Symphony X"), 2002, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa paradiseLost = new DiscoConImagenTapa("Paradise Lost", bs.getBanda("Symphony X"), 2007, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa iconoclast = new DiscoConImagenTapa("Iconoclast", bs.getBanda("Symphony X"), 2011, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa underworld = new DiscoConImagenTapa("Underworld", bs.getBanda("Symphony X"), 2015, ds.getDiscografica("Nuclear Blast"));
			
			DiscoConImagenTapa polars = new DiscoConImagenTapa("Polars", bs.getBanda("Textures"), 2003, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa drawingCircles = new DiscoConImagenTapa("Drawing Circles", bs.getBanda("Textures"), 2006, ds.getDiscografica("Nuclear Blast"));
			
			DiscoConImagenTapa undertow = new DiscoConImagenTapa("Undertow", bs.getBanda("Tool"), 1993, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa aenima = new DiscoConImagenTapa("Ænima", bs.getBanda("Tool"), 1996, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa lateralus = new DiscoConImagenTapa("Lateralus", bs.getBanda("Tool"), 2001, ds.getDiscografica("Nuclear Blast"));
			DiscoConImagenTapa tenKdays = new DiscoConImagenTapa("10,000 Day", bs.getBanda("Tool"), 2006, ds.getDiscografica("Nuclear Blast"));
			
			DiscoConImagenTapa one = new DiscoConImagenTapa("One", bs.getBanda("Tesseract"), 2010, ds.getDiscografica("Season of Mist"));
			
			DiscoConImagenTapa facelift = new DiscoConImagenTapa("Facelift", bs.getBanda("Alice in Chains"), 1990, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa dirt = new DiscoConImagenTapa("Dirt", bs.getBanda("Alice in Chains"), 1992, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa aliceInChains = new DiscoConImagenTapa("Alice In Chains", bs.getBanda("Alice in Chains"), 1995, ds.getDiscografica("Sub Pop Records"));
			
			DiscoConImagenTapa bleach = new DiscoConImagenTapa("Bleach", bs.getBanda("Nirvana"), 1989, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa nevermind = new DiscoConImagenTapa("Nevermind", bs.getBanda("Nirvana"), 1991, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa incesticide = new DiscoConImagenTapa("Incesticide", bs.getBanda("Nirvana"), 1992, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa inUtero = new DiscoConImagenTapa("In Utero", bs.getBanda("Nirvana"), 1993, ds.getDiscografica("Sub Pop Records"));
			
			DiscoConImagenTapa ten = new DiscoConImagenTapa("Ten", bs.getBanda("Pearl Jam"), 1991, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa vs = new DiscoConImagenTapa("Vs", bs.getBanda("Pearl Jam"), 1993, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa vitalogy = new DiscoConImagenTapa("Vitalogy", bs.getBanda("Pearl Jam"), 1994, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa noCode = new DiscoConImagenTapa("No Code", bs.getBanda("Pearl Jam"), 1996, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa yield = new DiscoConImagenTapa("Yield", bs.getBanda("Pearl Jam"), 1998, ds.getDiscografica("Sub Pop Records"));
			
			DiscoConImagenTapa ultramegaOK = new DiscoConImagenTapa("Ultramega OK", bs.getBanda("Soundgarden"), 1988, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa louderThanLove = new DiscoConImagenTapa("Louder Than Love", bs.getBanda("Soundgarden"), 1989, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa badmotorfinger = new DiscoConImagenTapa("Badmotorfinger", bs.getBanda("Soundgarden"), 1991, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa superunknown = new DiscoConImagenTapa("Superunknown", bs.getBanda("Soundgarden"), 1994, ds.getDiscografica("Sub Pop Records"));
			DiscoConImagenTapa downOnTheUpside = new DiscoConImagenTapa("Down on the Upside", bs.getBanda("Soundgarden"), 1996, ds.getDiscografica("Sub Pop Records"));
			
			DiscoConImagenTapa core = new DiscoConImagenTapa("Core", bs.getBanda("Stone Temple Pilots"), 1992, ds.getDiscografica("Sub Pop Records"));
			
			obscura.agregarCopiasVendidas(ps.getPais("Argentina"), 10000);
			
			reignInBlood.agregarCopiasVendidas(ps.getPais("Argentina"), 10000);
			
			tracedInAir.agregarCopiasVendidas(ps.getPais("Argentina"), 10000);
			
			superunknown.agregarCopiasVendidas(ps.getPais("Argentina"), 10000);
			nevermind.agregarCopiasVendidas(ps.getPais("Argentina"), 10000);
			
			this.discos = new HashSet<>(Arrays.asList(concealed, fragmentaryEvidence, leprosy,
					spiritualHealing, human, symbolic, consideredDead, erosionOfSanity, obscura,
					wisdom2hate, coloredSands, syzygialMiscreancy, omnipotentConvocation, hopelessHopes,
					warpZone, feedingTheAbscess, paleDebilitatingAutumn, furioso, arise, chaosAD, roots,
					against, hellAwaits, reignInBlood, southOfHeaven, redAlbum, blueRecord, focus,
					tracedInAir, kindlyBent, imagesNWords, awake, fallingIntoInf, metroPt2, sixDegrees,
					trainOfThought, octavarium, systematicChaos, remission, leviathan, bloodMountain,
					crackTheSkye, hunter, damnationGame, divineWings, twilightInOlympus, newMythologySuite,
					oddysey, paradiseLost, iconoclast, underworld, polars, drawingCircles, undertow,
					aenima, lateralus, tenKdays, one, facelift, dirt, aliceInChains, bleach, nevermind,
					incesticide, inUtero, ten, vs, vitalogy, noCode, yield, ultramegaOK, louderThanLove,
					badmotorfinger, superunknown, downOnTheUpside, core));

			for (DiscoConImagenTapa disco : this.discos) {
				disco.setPathLogo("disco_" + this.fixFileName(disco.getBanda().getNombre())
					+ "_" + this.fixFileName(disco.getNombre()) + ".jpg");
			}
		}
	}
	
	private String fixFileName(String nombre) {
		return nombre.toLowerCase().replaceAll("[^a-zA-Z0-9-]", "_");
	}

	// consultas
	
	public Disco getDisco(String nombre) {
		return this.discos.stream()
				.filter(disco -> disco.getNombre().equals(nombre))
				.findAny()
				.get();
	}

}
