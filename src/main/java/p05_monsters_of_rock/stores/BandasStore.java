package p05_monsters_of_rock.stores;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import org.uqbar.commons.utils.Observable;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.ui.BandaConImagenLogo;

@Observable
public class BandasStore {

	private static BandasStore instancia;
	private Collection<BandaConImagenLogo> bandas;
	
	// constructores
	
	private BandasStore() {
		super();
	}
	
	// instancia singleton
	
	public static BandasStore getInstancia() {
		if (instancia == null) {
			instancia = new BandasStore();
		}
		return instancia;
	}
	
	// datos
	
	public void cargarDatosPrueba() {
		if (this.bandas == null) {
			PaisesStore ps = PaisesStore.getInstancia();
			ps.cargarDatosPrueba();
			
			BandaConImagenLogo augury = new BandaConImagenLogo("Augury", "metal", 50000, ps.getPais("Canadá"));
			BandaConImagenLogo death = new BandaConImagenLogo("Death", "metal", 150000, ps.getPais("EEUU"));
			BandaConImagenLogo gorguts = new BandaConImagenLogo("Gorguts", "metal", 125000, ps.getPais("Canadá"));
			BandaConImagenLogo hellwitch =  new BandaConImagenLogo("Hellwitch", "metal", 75000, ps.getPais("EEUU"));
			BandaConImagenLogo martyr = new BandaConImagenLogo("Martyr", "metal", 50000, ps.getPais("Canadá"));
			BandaConImagenLogo pavor = new BandaConImagenLogo("Pavor", "metal", 75000, ps.getPais("Alemania"));
			BandaConImagenLogo sepultura = new BandaConImagenLogo("Sepultura", "metal", 80000, ps.getPais("Brasil"));
			BandaConImagenLogo slayer = new BandaConImagenLogo("Slayer", "metal", 125000, ps.getPais("EEUU"));
			
			BandaConImagenLogo baroness = new BandaConImagenLogo("Baroness", "rock", 50000, ps.getPais("EEUU"));
			BandaConImagenLogo cynic = new BandaConImagenLogo("Cynic", "rock", 100000, ps.getPais("EEUU"));
			BandaConImagenLogo dreamTheater = new BandaConImagenLogo("Dream Theater", "rock", 120000, ps.getPais("EEUU"));
			BandaConImagenLogo mastodon = new BandaConImagenLogo("Mastodon", "rock", 80000, ps.getPais("EEUU"));
			BandaConImagenLogo symphonyX = new BandaConImagenLogo("Symphony X", "rock", 90000, ps.getPais("EEUU"));
			BandaConImagenLogo textures = new BandaConImagenLogo("Textures", "rock", 50000, ps.getPais("Países Bajos"));
			BandaConImagenLogo tool = new BandaConImagenLogo("Tool", "rock", 75000, ps.getPais("EEUU"));
			
			BandaConImagenLogo tesseract = new BandaConImagenLogo("Tesseract", "djent", 7500, ps.getPais("Inglaterra"));
			
			BandaConImagenLogo aliceInChains = new BandaConImagenLogo("Alice in Chains", "grunge", 90000, ps.getPais("EEUU"));
			BandaConImagenLogo nirvana = new BandaConImagenLogo("Nirvana", "grunge", 100000, ps.getPais("EEUU"));
			BandaConImagenLogo pearlJam = new BandaConImagenLogo("Pearl Jam", "grunge", 90000, ps.getPais("EEUU"));
			BandaConImagenLogo soundgarden = new BandaConImagenLogo("Soundgarden", "grunge", 100000, ps.getPais("EEUU"));
			BandaConImagenLogo stoneTemplePilots = new BandaConImagenLogo("Stone Temple Pilots", "grunge", 90000, ps.getPais("EEUU"));
			
			this.bandas = new HashSet<>(Arrays.asList(augury, death, gorguts, hellwitch, martyr, pavor,
					sepultura, slayer, baroness, cynic, dreamTheater, mastodon, symphonyX, textures, tool,
					tesseract, aliceInChains, nirvana, pearlJam, soundgarden, stoneTemplePilots));
			
			for (BandaConImagenLogo banda : this.bandas) {
				banda.setPathLogo("banda_" + banda.getNombre().toLowerCase().replaceAll("[^a-zA-Z0-9.-]", "_") + ".png");
			}
		}
	}
	
	// consultas
	
	public List<Banda> getBandasOrdenNombre() {
		return this.bandas.stream()
				.sorted(Comparator.comparing(banda -> banda.getNombre()))
				.collect(Collectors.toList());
	}
	
	public Banda getBanda(String nombre) {
		return this.bandas.stream()
				.filter(banda -> banda.getNombre().equals(nombre))
				.findAny()
				.get();
	}
	
}
