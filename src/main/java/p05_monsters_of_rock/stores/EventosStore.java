package p05_monsters_of_rock.stores;

import java.util.Arrays;
import java.util.Collection;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.stream.Collectors;

import p05_monsters_of_rock.modelo.CenaShow;
import p05_monsters_of_rock.modelo.Evento;
import p05_monsters_of_rock.modelo.Festival;
import p05_monsters_of_rock.modelo.Recital;

public class EventosStore {
	
	private static EventosStore instancia;
	private Collection<Evento> eventos;
	
	// constructores
	
	private EventosStore() {
		super();
	}
	
	// instancia singleton
	
	public static EventosStore getInstancia() {
		if (instancia == null) {
			instancia = new EventosStore();
		}
		return instancia;
	}
	
	// datos
	
	public void cargarDatosPrueba() {
		if (this.eventos == null) {
			DiscograficasStore dgs = DiscograficasStore.getInstancia();
			dgs.cargarDatosPrueba();
			
			CiudadesStore cs = CiudadesStore.getInstancia();
			cs.cargarDatosPrueba();
			
			Festival festivalMetal1 = new Festival("Festival Metal Season of Mist", dgs.getDiscografica("Season of Mist"), 0, 100000);
			Festival festivalMetal2 = new Festival("Festival Metal Nuclear Blast", dgs.getDiscografica("Nuclear Blast"), 0, 100000);
			Recital recitalRock1 = new Recital("Recital Rock Nuclear Blast", dgs.getDiscografica("Nuclear Blast"), 0, 30000);
			Recital recitalRock2 = new Recital("Recital Rock Canadá", cs.getCiudad("Montreal"), 0, 30000);
			Recital recitalGrunge = new Recital("Recital Grunge", dgs.getDiscografica("Sub Pop Records"), 0, 50000);
			CenaShow cenaShow = new CenaShow("Cena-Show Season of Mist", dgs.getDiscografica("Season of Mist"), 0, 15000); 
			
			SedesStore ss = SedesStore.getInstancia();
			ss.cargarDatosPrueba();
			
			festivalMetal1.agregarSede(ss.getSede("Estadio Marcelo Bielsa"));
			festivalMetal2.agregarSede(ss.getSede("El Gigante de Alberdi"));
			recitalRock1.agregarSede(ss.getSede("Estadio Marcelo Bielsa"));
			recitalRock2.agregarSede(ss.getSede("Stade Olympique"));
			recitalGrunge.agregarSede(ss.getSede("Anfiteatro Martín Fierro"));
			cenaShow.agregarSede(ss.getSede("Anfiteatro Eva Perón"));
			
			festivalMetal1.agregarGeneroPermitido("metal");
			festivalMetal2.agregarGeneroPermitido("metal");
	
			festivalMetal1.agregarAporte("Coca-Cola", 50000);
			festivalMetal1.agregarAporte("Cerveza Quilmes", 50000);
			festivalMetal1.agregarAporte("Ford", 50000);
			festivalMetal1.agregarAporte("CBSé", 50000);
			
			festivalMetal2.agregarAporte("Milka", 50000);
			festivalMetal2.agregarAporte("Nestlé", 50000);
			festivalMetal2.agregarAporte("Toyota", 50000);
			festivalMetal2.agregarAporte("Taragüí", 50000);
			
			BandasStore bs = BandasStore.getInstancia();
			bs.cargarDatosPrueba();
			
			DiscosStore ds = DiscosStore.getInstancia();
			ds.cargarDatosPrueba();
			
			bs.getBanda("Gorguts").agregarseAEvento(festivalMetal1, ds.getDisco("Obscura"), 45);
			bs.getBanda("Sepultura").agregarseAEvento(festivalMetal1, ds.getDisco("Chaos A.D."), 35);
			bs.getBanda("Hellwitch").agregarseAEvento(festivalMetal1, ds.getDisco("Syzygial Miscreancy"), 40);
			
			bs.getBanda("Slayer").agregarseAEvento(festivalMetal2, ds.getDisco("Reign in Blood"), 60);
			bs.getBanda("Pavor").agregarseAEvento(festivalMetal2, ds.getDisco("A Pale Debilitating Autumn"), 45);
			
			bs.getBanda("Cynic").agregarseAEvento(recitalRock1, ds.getDisco("Traced in Air"), 40);
			bs.getBanda("Symphony X").agregarseAEvento(recitalRock1, ds.getDisco("V: The New Mythology Suite"), 30);
			
			bs.getBanda("Soundgarden").agregarseAEvento(recitalGrunge, ds.getDisco("Superunknown"), 50);
			bs.getBanda("Alice in Chains").agregarseAEvento(recitalGrunge, ds.getDisco("Facelift"), 45);
			bs.getBanda("Pearl Jam").agregarseAEvento(recitalGrunge, ds.getDisco("Ten"), 45);
			
			bs.getBanda("Tesseract").agregarseAEvento(cenaShow, ds.getDisco("One"), 30);
			
			this.eventos = new HashSet<>(Arrays.asList(cenaShow, festivalMetal1, festivalMetal2,
					recitalRock1, recitalRock2, recitalGrunge));
		}
	}
	
	// consultas
	
	public List<Evento> getEventosOrdenNombre() {
		return this.eventos.stream()
				.sorted(Comparator.comparing(evento -> evento.getNombre()))
				.collect(Collectors.toList());
	}
	
	public Evento getEvento(String nombre) {
		return this.eventos.stream()
				.filter(evento -> evento.getNombre().equals(nombre))
				.findAny()
				.get();
	}

}
