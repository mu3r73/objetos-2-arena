package p05_monsters_of_rock.ui;

import org.uqbar.arena.windows.MessageBox;
import org.uqbar.arena.windows.Window;

/**
 * table abstracta editable - agrega controles a una ventana para actuar sobre la lista model-object
 */
public abstract class TableSeleccionable<M, T> {

	protected Window<M> win; // ventana donde se agregarán los controles
	protected CollectionConSeleccion<T> ccs; // model-object
	
	// constructores
	
	public TableSeleccionable(Window<M> win, CollectionConSeleccion<T> ccs) {
		this.win = win;
		this.ccs = ccs;
	}
	
	// acciones
		
	/**
	 * muestra un diálogo de error
	 * @param error el mensaje de error a mostrar
	 */
	protected void mostrarError(String error) {
		MessageBox mb = new MessageBox(this.win, MessageBox.Type.Error);
		mb.setMessage(error);
		mb.open();
	}
	
}
