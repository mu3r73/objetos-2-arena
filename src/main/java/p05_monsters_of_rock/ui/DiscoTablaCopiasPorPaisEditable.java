package p05_monsters_of_rock.ui;

import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.List;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.Window;
import org.uqbar.commons.model.ObservableUtils;

import p05_monsters_of_rock.modelo.CopiasPorPais;
import p05_monsters_of_rock.modelo.Disco;
import p05_monsters_of_rock.modelo.Pais;
import p05_monsters_of_rock.stores.PaisesStore;

/**
 * disco tabla copias por país editable - agrega controles a una ventana para ver la lista de
 * copias vendidas de un disco por país,
 * que permite agregar, modificar y borrar items
 * (usada por DiscoWindow)
 */
public class DiscoTablaCopiasPorPaisEditable extends TableSeleccionable<Disco, CopiasPorPais> {

	// constructores
	
	public DiscoTablaCopiasPorPaisEditable(Window<Disco> win, CollectionConSeleccion<CopiasPorPais> ccs) {
		super(win, ccs);
	}
	
	// acciones
	
	/**
	 * agrega controles a una ventana para ver una tabla de copias vendidas de un disco por país editable
	 * @param panel el panel donde se van a agregar los controles
	 */
	public void agregarContenidos(Panel panel) {
		this.agregarTablaCopiasPorPais(panel);
		
		this.agregarBotones(panel);		
	}
	
	/**
	 * agrega una tabla con la lista de copias vendidas por país model-object de la ventana
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarTablaCopiasPorPais(Panel panel) {
		Table<CopiasPorPais> tabla = new Table<>(panel, CopiasPorPais.class);
		tabla
			.setNumberVisibleRows(5)
			.bindItemsToProperty("items");
		tabla.bindSelectionToProperty("itemSeleccionado");
		
		MoRArenaBuilder.agregarColumnaTituloPropiedadATabla(tabla, "Pais", "pais.nombre");
		MoRArenaBuilder.agregarColumnaTituloPropiedadATabla(tabla, "Copias", "copias");
	}
	
	/**
	 * agrega botones para agregar/modificar/borrar copias vendidas en un país
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotones(Panel panel) {
		Panel subPanel = new Panel(panel);
		subPanel.setLayout(new HorizontalLayout());
		
		this.agregarBotonAgregarCopiasVendidas(subPanel);
		this.agregarBotonModificarCopiasVendidas(subPanel);
		this.agregarBotonBorrarCopiasVendidas(subPanel);
	}

	/**
	 * agrega un botón, que al ser presionado, abrirá un diálogo
	 * para agregar copias vendidas en un país
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotonAgregarCopiasVendidas(Panel panel) {
		new Button(panel)
			.setCaption("Agregar")
			.onClick(() -> {
				this.abrirDialogoAgregarCopiasPorPais(panel);
			});
	}
	
	/**
	 * abre un diálogo para agregar copias vendidas en un país
	 * @param disco el disco seleccionado
	 */
	@SuppressWarnings("serial")
	private void abrirDialogoAgregarCopiasPorPais(Panel panel) {
		CopiasPorPais cpp = new CopiasPorPais();
		
		DialogConAceptarCancelar<CopiasPorPais> dialogo =
				new DialogConAceptarCancelar<CopiasPorPais>(this.win, cpp) {
			@Override
			protected void agregarContenidos(Panel panel) {
				this.setTitle("Agregar");
				
				this.agregarSelector(panel);
				
				this.agregarCantidad(panel);
			}
			
			/**
			 * agrega selector tipo list al diálogo, para seleccionar un país de la lista
			 * @param panel el panel donde se van a agregar los controles
			 */
			private void agregarSelector(Panel panel) {
				new Label(panel)
					.setText("País:");
				
				List<CopiasPorPais> selector = new List<>(panel);
				selector
					.setWidth(250)
					.setHeight(250)
					.bindValueToProperty("pais");
				selector
					.bindItems(new ObservableProperty<>(PaisesStore.getInstancia(), "paisesOrdenNombre"))
					.setAdapter(new PropertyAdapter(Pais.class, "nombre"));
			}
			
			/**
			 * agrega controles para seleccionar la cantidad de discos vendida
			 * @param panel el panel donde se van a agregar los controles
			 */
			private void agregarCantidad(Panel panel) {
				Panel subPanel = new Panel(panel);
				subPanel.setLayout(new ColumnLayout(2));
				
				MoRArenaBuilder.agregarLabelTituloSpinnerBindingDatoAPanel(subPanel, "Copias:", "copias");
			}

		};
		
		dialogo.onAccept(() -> {
			try {
				this.checkHayPaisSeleccionado(cpp);
				
				this.win.getModelObject().agregarCopiasVendidas(cpp);
				ObservableUtils.firePropertyChanged(this.ccs, "items");
				ObservableUtils.firePropertyChanged(this.win.getModelObject(), "totalCopiasVendidas");
				ObservableUtils.firePropertyChanged(this.win.getModelObject().getBanda(), "totalCopiasVendidas");

			} catch (RuntimeException e) {
				this.mostrarError(e.getLocalizedMessage());
			}
		});
		dialogo.open();
	}

	private void checkHayPaisSeleccionado(CopiasPorPais cpp) {
		if (cpp.getPais() == null) {
			throw new RuntimeException("no se seleccionó país");
		}
	}
	
	/**
	 * agrega un botón, que al ser presionado, abrirá un diálogo
	 * para modificar las copias vendidas en el país seleccionado
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotonModificarCopiasVendidas(Panel panel) {
		new Button(panel)
			.setCaption("Modificar")
			.onClick(() -> {
				this.abrirDialogoModificarCopiasPorPais();
			});
	}

	/**
	 * abre un diálogo para modificar las copias vendidas en el país seleccionado
	 * @param disco el disco seleccionado
	 */
	@SuppressWarnings("serial")
	private void abrirDialogoModificarCopiasPorPais() {
		try {
			this.checkHayPaisSeleccionado();
			
			CopiasPorPais cpp = new CopiasPorPais(this.ccs.getItemSeleccionado().getPais(), 
					this.ccs.getItemSeleccionado().getCopias());
			
			DialogConAceptarCancelar<CopiasPorPais> dialogo = 
					new DialogConAceptarCancelar<CopiasPorPais>(this.win, cpp) {
						
				@Override
				protected void agregarContenidos(Panel panel) {
					this.setTitle("Modificar");
					
					this.agregarPais(panel);
					
					this.agregarCantidad(panel);
				}
				
				/**
				 * agrega labels con información sobre el país
				 * @param panel el panel donde se van a agregar los controles
				 */
				private void agregarPais(Panel panel) {
					Panel subPanel = new Panel(panel);
					subPanel.setLayout(new ColumnLayout(2));
					
					MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "País:", "pais.nombre");
				}
				
				/**
				 * agrega controles para seleccionar la cantidad de discos vendida
				 * @param panel el panel donde se van a agregar los controles
				 */
				private void agregarCantidad(Panel panel) {
					Panel subPanel = new Panel(panel);
					subPanel.setLayout(new ColumnLayout(2));
					
					MoRArenaBuilder.agregarLabelTituloSpinnerBindingDatoAPanel(subPanel, "Copias:", "copias");
				}
	
			};
			
			dialogo.onAccept(() -> {
				this.ccs.getItemSeleccionado().setCopias(cpp.getCopias());
				ObservableUtils.firePropertyChanged(this.win.getModelObject(), "totalCopiasVendidas");
				ObservableUtils.firePropertyChanged(this.win.getModelObject().getBanda(), "totalCopiasVendidas");
			});
			dialogo.open();
			
		} catch (RuntimeException e) {
			this.mostrarError(e.getLocalizedMessage());
		}
	}

	private void checkHayPaisSeleccionado() {
		if (!this.ccs.hayItemSeleccionado()) {
			throw new RuntimeException("no se seleccionó país");
		}
	}

	/**
	 * agrega un botón, que al ser presionado, abrirá un diálogo
	 * para borrar las copias vendidas en el país seleccionado
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotonBorrarCopiasVendidas(Panel panel) {
		new Button(panel)
			.setCaption("Eliminar")
			.onClick(() -> {
				this.abrirDialogoBorrarCopiasPorPais();
			});
	}

	/**
	 * abre un diálogo para borrar las copias vendidas en el país seleccionado
	 * @param disco el disco seleccionado
	 */
	@SuppressWarnings("serial")
	private void abrirDialogoBorrarCopiasPorPais() {
		try {
			this.checkHayPaisSeleccionado();
			
			DialogConAceptarCancelar<CopiasPorPais> dialogo =
					new DialogConAceptarCancelar<CopiasPorPais>(this.win, this.ccs.getItemSeleccionado()) {
						
				protected void agregarContenidos(Panel panel) {
					this.setTitle("Eliminar");
					
					this.agregarCopiasPorPais(panel);
				}
				
				/**
				 * agrega labels con información del país y copias vendidas a borrar
				 * @param panel el panel donde se van a agregar los controles
				 */
				private void agregarCopiasPorPais(Panel panel) {
					Panel subPanel = new Panel(panel);
					subPanel.setLayout(new ColumnLayout(2));
					
					MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "País:", "pais.nombre");
					MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Copias:", "copias");
				}
				
			};
			
			dialogo.onAccept(() -> {
				this.win.getModelObject().borrarCopiasVendidas(this.ccs.getItemSeleccionado());
				ObservableUtils.firePropertyChanged(this.ccs, "items");
				ObservableUtils.firePropertyChanged(this.win.getModelObject(), "totalCopiasVendidas");
				ObservableUtils.firePropertyChanged(this.win.getModelObject().getBanda(), "totalCopiasVendidas");
			});
			dialogo.open();
			
		} catch (RuntimeException e) {
			this.mostrarError(e.getLocalizedMessage());
		}
	}
	
}
