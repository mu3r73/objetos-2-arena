package p05_monsters_of_rock.ui;

import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;

import p05_monsters_of_rock.modelo.Banda;

/**
 * ventana modal para ver información de una banda 
 */
@SuppressWarnings("serial")
public class BandaWindow extends Window<Banda> {

	public BandaWindow(WindowOwner owner, Banda model) {
		super(owner, model);
	}

	@Override
	public void createContents(Panel mainPanel) {
		new BandaPanelContenidos(this)
				.agregarContenidos(mainPanel);
	}

}
