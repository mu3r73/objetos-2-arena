package p05_monsters_of_rock.ui;

import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.MainWindow;

import p05_monsters_of_rock.modelo.Evento;

/**
 * muestra una ventana con una tabla que permite seleccionar un evento
 * (de la CollectionConSeleccion<Evento> para ver.
 */
@SuppressWarnings("serial")
public class SelectorEventoMainWindow extends MainWindow<CollectionConSeleccion<Evento>> {

	// constructores
	
	public SelectorEventoMainWindow(CollectionConSeleccion<Evento> model) {
		super(model);
	}

	// acciones
	
	@Override
	public void createContents(Panel mainPanel) {
		this.setTitle("Seleccionar evento");
		
		this.agregarTablaEventos(mainPanel);
	}

	private void agregarTablaEventos(Panel panel) {
		new SelectorEventoTablaEventos(this, this.getModelObject())
			.agregarContenidos(panel);		
	}

}
