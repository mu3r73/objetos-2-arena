package p05_monsters_of_rock.ui;

import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.Window;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Disco;

/**
 * banda tabla discos editable - agrega controles a una ventana para ver una lista de discos editable
 * que permite agregar, modificar y borrar copias vendidas por país
 * (usada por BandaPanelContenidos)
 */
public class BandaTablaDiscosEditable extends TableSeleccionable<Banda, Disco> {

	// constructores
	
	public BandaTablaDiscosEditable(Window<Banda> win, CollectionConSeleccion<Disco> ccs) {
		super(win, ccs);
	}
	
	// acciones
	
	/**
	 * agrega controles a una ventana para ver una lista de discos editable
	 * @param panel el panel donde se van a agregar los controles
	 */
	public void agregarContenidos(Panel panel) {
		this.agregarTablaDiscos(panel);

		this.agregarConsultaPorDiscoElegido(panel);
	}
	
	/**
	 * agrega una tabla con los discos de la lista model-object de la ventana
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarTablaDiscos(Panel panel) {
		Table<Disco> tabla = new Table<>(panel, Disco.class);
		tabla
			.setNumberVisibleRows(5)
			.bindItemsToProperty("items");
		tabla.bindSelectionToProperty("itemSeleccionado");
		
		MoRArenaBuilder.agregarColumnaTituloPropiedadATabla(tabla, "Nombre", "nombre");
		MoRArenaBuilder.agregarColumnaTituloPropiedadATabla(tabla, "Copias totales", "totalCopiasVendidas");
	}
	
	/**
	 * agrega un botón, que al ser presionado, abrirá una ventana
	 * con los detalles del disco seleccionado
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarConsultaPorDiscoElegido(Panel panel) {
		new Button(panel)
			.setCaption("Ver disco").onClick(() -> {
				this.abrirDetalleDisco(this.ccs.getItemSeleccionado());
			});
	}
	
	/**
	 * abre una ventana con los detalles del disco seleccionado
	 * @param disco el disco seleccionado
	 */
	private void abrirDetalleDisco(Disco disco) {
		try {
			this.checkHayDiscoSeleccionado();
			
			new DiscoWindow(this.win, disco)
				.open();
			
		} catch (Exception e) {
			this.mostrarError(e.getLocalizedMessage());
		}
	}

	private void checkHayDiscoSeleccionado() {
		if (!this.ccs.hayItemSeleccionado()) {
			throw new RuntimeException("no se seleccionó disco");
		}
	}
	
}
