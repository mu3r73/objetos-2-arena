package p05_monsters_of_rock.ui;

import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.List;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.WindowOwner;
import org.uqbar.commons.model.ObservableUtils;

import p05_monsters_of_rock.modelo.Disco;

/**
 * diálogo abstracto para editar (agregar o modificar) una presentación de un evento
 */
@SuppressWarnings("serial")
public abstract class EventoEditarPresentacionDialog extends DialogConAceptarCancelar<AltaPresentacion> {
	
	// constructor
	
	public EventoEditarPresentacionDialog(WindowOwner owner, AltaPresentacion model) {
		super(owner, model);
	}
	
	// acciones
	
	@Override
	protected void agregarContenidos(Panel panel) {
		this.agregarBanda(panel);
		
		this.agregarSelectorDisco(panel);
		
		this.agregarDuracion(panel);
	}
	
	/**
	 * agrega información sobre la banda de la presentación
	 * @param panel el panel donde se van a agregar los controles
	 */
	protected abstract void agregarBanda(Panel panel);
	
	/**
	 * agrega selector tipo list al diálogo, para seleccionar un disco de la banda
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarSelectorDisco(Panel panel) {
		new Label(panel)
			.setText("Disco:")
			.bindVisibleToProperty("hayBandaSeleccionada");
		
		List<Disco> selector = new List<>(panel);
		selector
			.setWidth(250)
			.setHeight(150)
			.bindValueToProperty("disco");
		selector
			.bindItemsToProperty("discosDeBanda")
			.setAdapter(new PropertyAdapter(Disco.class, "nombre"));
		
		selector.bindVisibleToProperty("hayBandaSeleccionada");
		
		ObservableUtils.dependencyOf(this.getModelObject(), "discosDeBanda", "banda");
	}
	
	/**
	 * agrega controles para seleccionar la duración de la presentación
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarDuracion(Panel panel) {
		Panel subPanel = new Panel(panel);
		subPanel.setLayout(new ColumnLayout(2));
		
		MoRArenaBuilder.agregarLabelTituloSpinnerBindingDatoAPanel(subPanel, "Duración:", "duracion");
	}

}
