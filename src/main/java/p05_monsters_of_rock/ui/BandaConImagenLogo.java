package p05_monsters_of_rock.ui;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Pais;

public class BandaConImagenLogo extends Banda {

	private String pathLogo;
	
	public BandaConImagenLogo(String nombre, String genero, double cachet, Pais pais) {
		super(nombre, genero, cachet, pais);
	}
	
	public String getPathLogo() {
		return this.pathLogo;
	}
	
	public void setPathLogo(String path) {
		this.pathLogo = path;
	}

}
