package p05_monsters_of_rock.ui;

import org.uqbar.arena.bindings.ObservableProperty;
import org.uqbar.arena.bindings.PropertyAdapter;
import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.layout.HorizontalLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.List;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.Window;
import org.uqbar.commons.model.ObservableUtils;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Evento;
import p05_monsters_of_rock.modelo.Presentacion;
import p05_monsters_of_rock.stores.BandasStore;

/**
 * evento tabla presentaciones editable - agrega controles a una ventana para ver la lista de
 * presentaciones de un evento,
 * que permite agregar, modificar y borrar items
 * (usada por EventoPanelContenidos)
 */
public class EventoTablaPresentacionesEditable extends TableSeleccionable<Evento, Presentacion> {
	
	// constructores
	
	public EventoTablaPresentacionesEditable(Window<Evento> win, CollectionConSeleccion<Presentacion> ccs) {
		super(win, ccs);
	}
	
	// acciones
	
	/**
	 * agrega controles a una ventana para ver una tabla de presentaciones de un evento editable
	 * @param panel el panel donde se van a agregar los controles
	 */
	public void agregarContenidos(Panel panel) {
		this.agregarTablaPresentaciones(panel);
		
		this.agregarBotones(panel);
	}
	
	/**
	 * agrega una tabla con la lista de presentaciones model-object de la ventana
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarTablaPresentaciones(Panel panel) {
		Table<Presentacion> tabla = new Table<>(panel, Presentacion.class);
		tabla
			.setNumberVisibleRows(5)
			.bindItemsToProperty("items");
		tabla.bindSelectionToProperty("itemSeleccionado");
		
		MoRArenaBuilder.agregarColumnaTituloPropiedadATabla(tabla, "Banda", "banda.nombre");
		MoRArenaBuilder.agregarColumnaTituloPropiedadATabla(tabla, "Disco", "disco.nombre");
		MoRArenaBuilder.agregarColumnaTituloPropiedadATabla(tabla, "Duración", "duracion");
	}
	
	/**
	 * agrega botones para ver información sobre una banda, o agregar/modificar/borrar una presentación
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotones(Panel panel) {
		Panel subPanel = new Panel(panel);
		subPanel.setLayout(new HorizontalLayout());
		
		this.agregarBotonVerBanda(subPanel);
		this.agregarBotonAgregarBanda(subPanel);
		this.agregarBotonModificarBanda(subPanel);
		this.agregarBotonBorrarBanda(subPanel);
	}
	
	/**
	 * agrega un botón, que al ser presionado, abrirá un diálogo
	 * para ver información sobre la banda seleccionada
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotonVerBanda(Panel panel) {
		new Button(panel)
			.setCaption("Ver banda").onClick(() -> {
				this.abrirVentanaBanda();				
			});
	}
	
	/**
	 * abre un diálogo para ver información sobre la banda seleccionada
	 * @param disco el disco seleccionado
	 */
	private void abrirVentanaBanda() {
		try {
			this.checkHayBandaSeleccionada();
			
			new BandaWindow(this.win, this.ccs.getItemSeleccionado().getBanda())
				.open();
		} catch (Exception e) {
			this.mostrarError(e.getLocalizedMessage());
		}
	}

	private void checkHayBandaSeleccionada() {
		if (!this.ccs.hayItemSeleccionado()) {
			throw new RuntimeException("no se seleccionó banda");
		}
	}
	
	/**
	 * agrega un botón, que al ser presionado, abrirá un diálogo
	 * para agregar una presentación
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotonAgregarBanda(Panel panel) {
		new Button(panel)
			.setCaption("Agregar banda")
			.onClick(() -> {
				this.abrirDialogoAgregarPresentacion();
			});
	}

	/**
	 * abre un diálogo para agregar una presentación
	 * @param disco el disco seleccionado
	 */
	@SuppressWarnings("serial")
	private void abrirDialogoAgregarPresentacion() {
		AltaPresentacion apres = new AltaPresentacion();
		
		DialogConAceptarCancelar<AltaPresentacion> dialogo = new EventoEditarPresentacionDialog(this.win, apres) {
			
			@Override
			protected void agregarContenidos(Panel panel) {
				this.setTitle("Agregar");
				
				super.agregarContenidos(panel);
			}
			
			@Override
			protected void agregarBanda(Panel panel) {
				this.agregarSelectorBanda(panel);
			}
			
			/**
			 * agrega selector tipo list al diálogo, para seleccionar una banda de la lista
			 * @param panel el panel donde se van a agregar los controles
			 */
			private void agregarSelectorBanda(Panel panel) {
				new Label(panel)
					.setText("Banda:");
				
				List<Presentacion> selector = new List<>(panel);
				selector
					.setWidth(250)
					.setHeight(200);
				selector
					.bindItems(new ObservableProperty<>(BandasStore.getInstancia(), "bandasOrdenNombre"))
					.setAdapter(new PropertyAdapter(Banda.class, "nombre"));
				selector.bindValueToProperty("banda");
			}
			
		};
		
		dialogo.onAccept(() -> {
			try {
				this.checkHayBandaSeleccionada(apres);
				this.checkHayDiscoSeleccionado(apres);
				
				apres.getBanda().agregarseAEvento(this.win.getModelObject(), apres.getDisco(), apres.getDuracion());
				
				ObservableUtils.firePropertyChanged(this.ccs, "items");
				ObservableUtils.firePropertyChanged(this.win.getModelObject(), "gastosBasicos");
				ObservableUtils.firePropertyChanged(this.win.getModelObject(), "duracionTotal");
			} catch (Exception e) {
				this.mostrarError(e.getLocalizedMessage());
			}
		});
		dialogo.open();
	}

	private void checkHayDiscoSeleccionado(AltaPresentacion apres) {
		if (!apres.tieneDisco()) {
			throw new RuntimeException("no se seleccionó disco");
		}
	}

	private void checkHayBandaSeleccionada(AltaPresentacion apres) {
		if (!apres.tieneBanda()) {
			throw new RuntimeException("no se seleccionó banda");
		}
	}

	/**
	 * agrega un botón, que al ser presionado, abrirá un diálogo
	 * para modificar una presentación
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotonModificarBanda(Panel panel) {
		new Button(panel)
			.setCaption("Modificar banda")
			.onClick(() -> {
				this.abrirDialogoModificarBanda();
			});
	}

	/**
	 * abre un diálogo para modificar la presentación seleccionada
	 * @param disco el disco seleccionado
	 */
	@SuppressWarnings("serial")
	private void abrirDialogoModificarBanda() {
		try {
			this.checkHayBandaSeleccionada();
			this.checkEsBandaPrincipalConBandasSoporte();
			
			Presentacion pres = this.ccs.getItemSeleccionado();
			AltaPresentacion apres = new AltaPresentacion(pres.getBanda(), pres.getDisco(), pres.getDuracion());
			
			DialogConAceptarCancelar<AltaPresentacion> dialogo = new EventoEditarPresentacionDialog(this.win, apres) {
				
				@Override
				protected void agregarContenidos(Panel panel) {
					this.setTitle("Agregar");
					
					super.agregarContenidos(panel);
				}
				
				/**
				 * agrega labels con información sobre la banda
				 * @param panel el panel donde se van a agregar los controles
				 */
				protected void agregarBanda(Panel panel) {
					Panel subPanel = new Panel(panel);
					subPanel.setLayout(new ColumnLayout(2));
					
					MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Banda:", "banda.nombre");
					
					this.getModelObject().setHayBandaSeleccionada(true);
				}
				
			};
			
			dialogo.onAccept(() -> {
				this.checkHayDiscoSeleccionado(apres);
				/*
				 * el código que sigue borra la presentación del evento...
				 * si NO se puede agregar la presentación modificada, restaura la presentación original;
				 * en caso contrario, agrega nueva presentación modificada -
				 * OJO: no funciona cuando se quiere modificar la banda principal y hay bandas soporte
				 */
				try {
					this.win.getModelObject().borrarPresentacion(pres);
					
					apres.getBanda().agregarseAEvento(this.win.getModelObject(), apres.getDisco(), apres.getDuracion());
					
					ObservableUtils.firePropertyChanged(this.ccs, "items");
					ObservableUtils.firePropertyChanged(this.win.getModelObject(), "gastosBasicos");
					ObservableUtils.firePropertyChanged(this.win.getModelObject(), "duracionTotal");
					
				} catch (Exception e) {
					pres.getBanda().agregarseAEvento(this.win.getModelObject(), pres.getDisco(), pres.getDuracion());
					this.mostrarError(e.getLocalizedMessage());
				}
			});
			dialogo.open();
			
		} catch (Exception e) {
			this.mostrarError(e.getLocalizedMessage());
		}
	}

	private void checkEsBandaPrincipalConBandasSoporte() {
		if (this.win.getModelObject().esBandaPrincipal(this.ccs.getItemSeleccionado().getBanda())
				&& this.win.getModelObject().tieneBandasSoporte()) {
			throw new RuntimeException("no se puede modificar ni eliminar la banda principal porque hay bandas soporte");
		}
	}
	
	/**
	 * agrega un botón, que al ser presionado, abrirá un diálogo
	 * para borrar la presentación seleccionada
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotonBorrarBanda(Panel panel) {
		new Button(panel)
			.setCaption("Eliminar banda")
			.onClick(() -> {
				this.abrirDialogoBorrarBanda();
			});
	}

	/**
	 * abre un diálogo para borrar la presentación seleccionada
	 * @param disco el disco seleccionado
	 */
	@SuppressWarnings("serial")
	private void abrirDialogoBorrarBanda() {
		try {
			this.checkHayBandaSeleccionada();
			this.checkEsBandaPrincipalConBandasSoporte();
			
			Presentacion pres = this.ccs.getItemSeleccionado();
			
			DialogConAceptarCancelar<Presentacion> dialogo = 
					new DialogConAceptarCancelar<Presentacion>(this.win, pres) {
				
				@Override
				protected void agregarContenidos(Panel panel) {
					this.setTitle("Eliminar");
					
					this.agregarPresentacion(panel);		
				}
				
				/**
				 * agrega labels con información sobre la presentación a borrar
				 * @param panel el panel donde se van a agregar los controles
				 */
				private void agregarPresentacion(Panel panel) {
					Panel subPanel = new Panel(panel);
					subPanel.setLayout(new ColumnLayout(2));
					
					MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Banda:", "banda.nombre");
					MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Disco:", "disco.nombre");
					MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Duración:", "duracion");
				}
						
			};
			
			dialogo.onAccept(() -> {
				this.win.getModelObject().borrarPresentacion(pres);
				ObservableUtils.firePropertyChanged(this.ccs, "items");
				ObservableUtils.firePropertyChanged(this.win.getModelObject(), "gastosBasicos");
				ObservableUtils.firePropertyChanged(this.win.getModelObject(), "duracionTotal");
			});
			dialogo.open();
			
		} catch (Exception e) {
			this.mostrarError(e.getLocalizedMessage());
		}
	} 
}
