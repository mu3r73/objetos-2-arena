package p05_monsters_of_rock.ui;

import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;

import p05_monsters_of_rock.modelo.Evento;

/**
 * ventana modal para ver información de un evento 
 */
@SuppressWarnings("serial")
public class EventoWindow extends Window<Evento> {

	public EventoWindow(WindowOwner owner, Evento model) {
		super(owner, model);
	}

	@Override
	public void createContents(Panel mainPanel) {
		new EventoPanelContenidos(this)
			.agregarContenidos(mainPanel);
	}

}
