package p05_monsters_of_rock.ui;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Disco;
import p05_monsters_of_rock.modelo.Discografica;

public class DiscoConImagenTapa extends Disco {

	private String pathLogo;

	public DiscoConImagenTapa(String nombre, Banda banda, int anioDeProduccion, Discografica discografica) {
		super(nombre, banda, anioDeProduccion, discografica);
	}

	public String getPathLogo() {
		return this.pathLogo;
	}
	
	public void setPathLogo(String path) {
		this.pathLogo = path;
	}
	
}
