package p05_monsters_of_rock.ui;

import java.util.ArrayList;
import java.util.List;

import org.uqbar.commons.utils.Observable;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Disco;
import p05_monsters_of_rock.modelo.Presentacion;

/**
 * alta de una presentación
 * información necesaria para dar de alta una presentación  
 */
@Observable
public class AltaPresentacion extends Presentacion {
	
	private boolean hayBandaSeleccionada = false;
	
	// constructores
	
	public AltaPresentacion() {
		super();
	}
	
	public AltaPresentacion(Banda banda, Disco disco, int duracion) {
		super(banda, disco, duracion);
	}
	
	// getters / setters
	
	@Override
	public void setBanda(Banda banda) {
		super.setBanda(banda);
		this.setHayBandaSeleccionada(true);
	}
	
	public boolean getHayBandaSeleccionada() {
		return this.hayBandaSeleccionada;
	}
	
	public void setHayBandaSeleccionada(boolean estado) {
		this.hayBandaSeleccionada = estado;
	}
	
	// consultas
	
	/**
	 * retorna los discos de la banda seleccionada, ordenados por año
	 * @return discos de la banda seleccionada, ordenados por año,
	 * o lista vacía si no hay banda seleccionada
	 */
	public List<Disco> getDiscosDeBanda() {
		if (this.getBanda() == null) {
			return new ArrayList<>();
		} else {
			return this.getBanda().getDiscosOrdenAnio();
		}
	}
	
	/**
	 * indica si se ha seleccionado una banda
	 * @return true o false 
	 */
	public boolean tieneBanda() {
		return this.getBanda() != null;
	}
	
	/**
	 * indica si se ha seleccionado un disco
	 * @return true o false
	 */
	public boolean tieneDisco() {
		return this.getDisco() != null;
	}

}
