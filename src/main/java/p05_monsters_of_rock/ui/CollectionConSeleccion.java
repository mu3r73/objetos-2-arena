package p05_monsters_of_rock.ui;

import java.util.Collection;
import java.util.List;

import org.uqbar.commons.utils.Observable;

/**
 * clase wrapper para una Collection, que además de los elementos,
 * mantiene un elegido entre ellos
 * @param <T> la clase de los datos de la Collection
 */
@Observable
public class CollectionConSeleccion<T> {

	private Collection<T> items;
	private T itemSeleccionado;
	
	// constructores
	
	public CollectionConSeleccion(Collection<T> items) {
		this.items = items;
	}

	// getters y setters
	
	public Collection<T> getItems() {
		return this.items;
	}

	public void setItems(List<T> items) {
		this.items = items;
	}

	public T getItemSeleccionado() {
		return this.itemSeleccionado;
	}
	
	public void setItemSeleccionado(T item) {
		this.itemSeleccionado = item;
	}
	
	// consultas
	
	/**
	 * indica si se ha seleccionado un item de la Collection
	 * @return true o false
	 */
	public boolean hayItemSeleccionado() {
		return this.itemSeleccionado != null;
	}
	
}
