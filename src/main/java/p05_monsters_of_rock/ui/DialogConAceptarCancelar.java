package p05_monsters_of_rock.ui;

import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Dialog;
import org.uqbar.arena.windows.WindowOwner;

/**
 * clase abstracta - diálogo con botones Aceptar y Cancelar  
 * @param <T> la clase de los datos vinculados al diálogo
 */
@SuppressWarnings("serial")
public abstract class DialogConAceptarCancelar<T> extends Dialog<T> {

	// constructores
	
	public DialogConAceptarCancelar(WindowOwner owner, T model) {
		super(owner, model);
	}

	// acciones
	
	@Override
	protected void createFormPanel(Panel mainPanel) {
		mainPanel.setLayout(new VerticalLayout());
		
		this.agregarContenidos(mainPanel);
		
		this.agregarBotones(mainPanel);
	}

	/**
	 * agrega controles al diálogo
	 * @param panel el panel donde se van a agregar los controles
	 */
	protected abstract void agregarContenidos(Panel panel);
	
	/**
	 * agrega botones Aceptar y Cancelar
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotones(Panel panel) {
		this.agregarBotonAceptar(panel);
		this.agregarBotonCancelar(panel);
	}
	
	/**
	 * agrega botón Aceptar
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotonAceptar(Panel panel) {
		new Button(panel)
			.setCaption("Aceptar")
			.setAsDefault()
			.onClick(() ->
				this.accept()
			);
	}

	/**
	 * agrega botón Cancelar
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarBotonCancelar(Panel panel) {
		new Button(panel)
			.setCaption("Cancelar")
			.onClick(() ->
				this.cancel()
			);
	}

}
