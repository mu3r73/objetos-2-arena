package p05_monsters_of_rock.ui;

import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Window;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Disco;

/**
 * banda panel de contenidos - agrega controles a una ventana para ver información de una banda
 * (usada por BandaMainWindow y BandaWindow)
 */
public class BandaPanelContenidos {
	
	private Window<Banda> win; // ventana a la que agregaremos contenidos
	
	// constructores
	
	public BandaPanelContenidos(Window<Banda> win) {
		this.win = win;
	}
	
	// acciones
	
	/**
	 * agrega controles a una ventana para ver información de la banda model-object de la ventana
	 * @param panel el panel donde se van a agregar los controles
	 */
	public void agregarContenidos(Panel panel) {
		this.win.setTitle("Banda");
		
		panel.setLayout(new VerticalLayout());
		
		this.agregarLogoBanda(panel);
		
		this.agregarDatosBanda(panel);
		
		this.agregarTablaDiscosEditable(panel);
	}
	
	/**
	 * agrega una label con el logo de la banda model-object de la ventana
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarLogoBanda(Panel panel) {
		MoRArenaBuilder.agregarLabelImagenAPanel(panel, "pathLogo");
	}
	
	/**
	 * agrega labels con información de la banda model-object de la ventana
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarDatosBanda(Panel panel) {
		Panel subPanel = new Panel(panel);
		subPanel.setLayout(new ColumnLayout(2));
		
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Nombre", "nombre");
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "País", "pais.nombre");
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Género", "genero");
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Copias vendidas (total)", "totalCopiasVendidas");
	}
	
	/**
	 * agrega una tabla editable con los discos de la banda model-object de la ventana 
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarTablaDiscosEditable(Panel panel) {
		CollectionConSeleccion<Disco> dcs =
				new CollectionConSeleccion<>(this.win.getModelObject().getDiscosOrdenAnio());
		Panel subPanel = new Panel(panel, dcs);
		new BandaTablaDiscosEditable(this.win, dcs)
			.agregarContenidos(subPanel);
	}
	
}
