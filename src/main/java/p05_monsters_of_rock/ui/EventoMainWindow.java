package p05_monsters_of_rock.ui;

import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.MainWindow;

import p05_monsters_of_rock.modelo.Evento;

/**
 * ventana para ver información de un evento 
 */
@SuppressWarnings("serial")
public class EventoMainWindow extends MainWindow<Evento> {

	public EventoMainWindow(Evento model) {
		super(model);
	}

	@Override
	public void createContents(Panel mainPanel) {
		new EventoPanelContenidos(this)
			.agregarContenidos(mainPanel);
	}

}
