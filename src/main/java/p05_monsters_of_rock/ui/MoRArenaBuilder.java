package p05_monsters_of_rock.ui;

import org.apache.commons.collections15.Transformer;
import org.uqbar.arena.graphics.Image;
import org.uqbar.arena.widgets.Label;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.Spinner;
import org.uqbar.arena.widgets.tables.Column;
import org.uqbar.arena.widgets.tables.Table;

public class MoRArenaBuilder {

	/**
	 * agrega un par label título - label con binding de datos
	 * @param titulo texto que describe una propiedad 
	 * @param propiedad propiedad observable
	 * @return el panel
	 */
	public static void agregarLabelTituloLabelBindingDatoAPanel(Panel panel, String titulo, String propiedad) {
		new Label(panel)
			.setText(titulo)
			.setWidth(200);
		
		new Label(panel)
			.setWidth(200)
			.bindValueToProperty(propiedad);
	}
	
	/**
	 * agrega un par label título - spinner con binding de datos numéricos
	 * @param titulo texto que describe una propiedad
	 * @param propiedad propiedad observable (número entero)
	 * @return el panel
	 */
	public static void agregarLabelTituloSpinnerBindingDatoAPanel(Panel panel, String titulo, String propiedad) {
		new Label(panel)
			.setText(titulo)
			.setWidth(100);
		
		Spinner spinner = new Spinner(panel);
		spinner.setWidth(120);
		spinner.setMinimumValue(0);
		spinner.setMaximumValue(1000000000);
		spinner.bindValueToProperty(propiedad);
	}
	
	/**
	 * agrega un label con binding de imagen
	 * @param propiedad propiedad observable (nombre de archivo de una imagen)
	 * @return el panel
	 */
	public static void agregarLabelImagenAPanel(Panel panel, String propiedad) {
		new Label(panel)
			.bindImageToProperty(propiedad, new Transformer<String, Image>() {
				@Override
				public Image transform(String path) {
					return new Image(path);
				}
			});
	}
	
	/**
	 * agrega una columna con título y con binding de datos 
	 * @param <R> tipo asociado a la tabla
	 * @param titulo título de la columna
	 * @param propiedad propiedad observable
	 * @return la table
	 */
	public static <R> void agregarColumnaTituloPropiedadATabla(Table<R> tabla, String titulo, String propiedad) {
		new Column<R>(tabla)
			.setTitle(titulo)
			.setFixedSize(220)
			.bindContentsToProperty(propiedad);
	}
	
}
