package p05_monsters_of_rock.ui;

import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.MainWindow;

import p05_monsters_of_rock.modelo.Banda;

/**
 * ventana para ver información de una banda 
 */
@SuppressWarnings("serial")
public class BandaMainWindow extends MainWindow<Banda> {

	public BandaMainWindow(Banda model) {
		super(model);
	}
	
	@Override
	public void createContents(Panel mainPanel) {
		new BandaPanelContenidos(this)
				.agregarContenidos(mainPanel);
	}

}
