package p05_monsters_of_rock.ui;

import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Window;

import p05_monsters_of_rock.modelo.Evento;
import p05_monsters_of_rock.modelo.Presentacion;

/**
 * evento panel de contenidos - agrega controles a una ventana para ver información de un evento
 * (usada por EventoMainWindow y EventoWindow)
 */
public class EventoPanelContenidos {
	
	private Window<Evento> win; // ventana a la que agregaremos contenidos
	
	// constructores
	
	public EventoPanelContenidos(Window<Evento> win) {
		this.win = win;
	}
	
	// acciones
	
	/**
	 * agrega controles a una ventana para ver información del evento model-object de la ventana
	 * @param panel el panel donde se van a agregar los controles
	 */
	public void agregarContenidos(Panel panel) {
		this.win.setTitle("Evento");
		
		panel.setLayout(new VerticalLayout());
		
		this.agregarDatosEvento(panel);

		this.agregarTablaPresentacionesEditable(panel);				
	}
	
	/**
	 * agrega labels con información del evento model-object de la ventana
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarDatosEvento(Panel panel) {
		Panel subPanel = new Panel(panel);
		subPanel.setLayout(new ColumnLayout(2));
		
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Nombre", "nombre");
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "País", "sede.pais.nombre");
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Ingreso asegurado", "ingresoAsegurado");
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Gastos básicos", "gastosBasicos");
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Duración", "duracionTotal");
	}
	
	/**
	 * agrega una tabla editable con las presentaciones del evento model-object de la ventana 
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarTablaPresentacionesEditable(Panel panel) {
		CollectionConSeleccion<Presentacion> pcs = 
				new CollectionConSeleccion<>(this.win.getModelObject().getPresentaciones());
		Panel subPanel = new Panel(panel, pcs);
		new EventoTablaPresentacionesEditable(this.win, pcs)
			.agregarContenidos(subPanel);
	}

}
