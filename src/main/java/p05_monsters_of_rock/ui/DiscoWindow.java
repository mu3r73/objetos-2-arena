package p05_monsters_of_rock.ui;

import org.uqbar.arena.layout.ColumnLayout;
import org.uqbar.arena.layout.VerticalLayout;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.windows.Window;
import org.uqbar.arena.windows.WindowOwner;

import p05_monsters_of_rock.modelo.CopiasPorPais;
import p05_monsters_of_rock.modelo.Disco;

/**
 * ventana modal para ver información de un disco 
 */
@SuppressWarnings("serial")
public class DiscoWindow extends Window<Disco> {
	
	// constructores
	
	public DiscoWindow(WindowOwner owner, Disco model) {
		super(owner, model);
	}

	// acciones
	
	@Override
	public void createContents(Panel mainPanel) {
		this.setTitle("Disco");
		
		mainPanel.setLayout(new VerticalLayout());
		
		this.agregarTapaDisco(mainPanel);
		
		this.agregarDatosDisco(mainPanel);
		
		this.agregarTablaCopiasPorPaisEditable(mainPanel);
	}
	
	/**
	 * agrega una label con la foto de tapa del disco model-object de la ventana
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarTapaDisco(Panel panel) {
		MoRArenaBuilder.agregarLabelImagenAPanel(panel, "pathLogo");
	}
	
	/**
	 * agrega labels con información del disco model-object de la ventana
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarDatosDisco(Panel panel) {
		Panel subPanel = new Panel(panel);
		subPanel.setLayout(new ColumnLayout(2));
		
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Nombre", "nombre");
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Año de edición", "anioDeEdicion");
		MoRArenaBuilder.agregarLabelTituloLabelBindingDatoAPanel(subPanel, "Copias vendidas (total)", "totalCopiasVendidas");
	}

	/**
	 * agrega una tabla editable con las copias vendidas por país de la banda model-object de la ventana 
	 * @param panel el panel donde se van a agregar los controles
	 */
	private void agregarTablaCopiasPorPaisEditable(Panel panel) {
		CollectionConSeleccion<CopiasPorPais> cppcs =
				new CollectionConSeleccion<>(this.getModelObject().getCopiasPorPais());
		Panel subPanel = new Panel(panel, cppcs);
		new DiscoTablaCopiasPorPaisEditable(this, cppcs)
			.agregarContenidos(subPanel);
	}

}
