package p05_monsters_of_rock.ui;

import org.uqbar.arena.widgets.Button;
import org.uqbar.arena.widgets.Panel;
import org.uqbar.arena.widgets.tables.Table;
import org.uqbar.arena.windows.Window;

import p05_monsters_of_rock.modelo.Evento;

public class SelectorEventoTablaEventos extends TableSeleccionable<CollectionConSeleccion<Evento>, Evento> {

	// constructores
	
	public SelectorEventoTablaEventos(Window<CollectionConSeleccion<Evento>> win, CollectionConSeleccion<Evento> ccs) {
		super(win, ccs);
	}
	
	// acciones
	
	/**
	 * agrega controles a una ventana para ver una lista de eventos seleccionables
	 * @param panel el panel donde se van a agregar los controles
	 */
	public void agregarContenidos(Panel panel) {
		this.agregarTablaEventos(panel);
		
		this.agregarConsultaPorEventoElegido(panel);
	}
	
	/**
	 * agrega una tabla que permite seleccionar un evento de la lista model-object
	 * @param panel el panel donde se va a agregar la tabla
	 */
	private void agregarTablaEventos(Panel panel) {
		Table<Evento> tabla = new Table<>(panel, Evento.class);
		tabla
			.setNumberVisibleRows(7)
			.bindItemsToProperty("items");
		tabla.bindSelectionToProperty("itemSeleccionado");
		
		MoRArenaBuilder.agregarColumnaTituloPropiedadATabla(tabla, "Evento", "nombre");
		MoRArenaBuilder.agregarColumnaTituloPropiedadATabla(tabla, "Organizador", "organizador.nombre");
	}
	
	/**
	 * agrega un botón, que al ser presionado, abrirá una ventana
	 * con los detalles del evento seleccionado
	 * @param panel el panel donde se va a agregar el botón
	 */
	private void agregarConsultaPorEventoElegido(Panel panel) {
		new Button(panel)
			.setCaption("Ver evento").onClick(() -> {
				this.abrirDetalleEvento(this.ccs.getItemSeleccionado());
			});
	}
	
	/**
	 * abre una ventana con los detalles del evento
	 * @param evento el evento seleccionado
	 */
	private void abrirDetalleEvento(Evento evento) {
		try {
			this.checkHayEventoSeleccionado();
			
			new EventoWindow(this.win, evento)
				.open();
			
		} catch (Exception e) {
			this.mostrarError(e.getLocalizedMessage());
		}
	}

	private void checkHayEventoSeleccionado() {
		if (!this.ccs.hayItemSeleccionado()) {
			throw new RuntimeException("no se seleccionó evento");
		}
	}
	
}
