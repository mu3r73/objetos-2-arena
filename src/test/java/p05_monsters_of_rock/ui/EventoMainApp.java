package p05_monsters_of_rock.ui;

import p05_monsters_of_rock.stores.EventosStore;
import p05_monsters_of_rock.ui.EventoMainWindow;

public class EventoMainApp {

	public static void main(String[] args) {
		
		EventosStore es = EventosStore.getInstancia();
		es.cargarDatosPrueba();

//		EventoMainWindow emw = new EventoMainWindow(es.getEvento("Cena-Show"));
//		EventoMainWindow emw = new EventoMainWindow(es.getEvento("Recital Grunge"));
//		EventoMainWindow emw = new EventoMainWindow(es.getEvento("Festival Metal Season of Mist"));
//		EventoMainWindow emw = new EventoMainWindow(es.getEvento("Festival Metal Nuclear Blast"));
		EventoMainWindow emw = new EventoMainWindow(es.getEvento("Recital Rock Nuclear Blast"));
		emw.startApplication();

	}
	
}
