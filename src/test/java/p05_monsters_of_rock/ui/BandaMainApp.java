package p05_monsters_of_rock.ui;

import p05_monsters_of_rock.stores.BandasStore;
import p05_monsters_of_rock.stores.DiscosStore;
import p05_monsters_of_rock.ui.BandaMainWindow;

public class BandaMainApp {

	public static void main(String[] args) {
		
		BandasStore bs = BandasStore.getInstancia();
		bs.cargarDatosPrueba();
		
		DiscosStore ds = DiscosStore.getInstancia();
		ds.cargarDatosPrueba();
		
		BandaMainWindow bmw = new BandaMainWindow(bs.getBanda("Gorguts"));

		bmw.startApplication();
		
	}
	
}
