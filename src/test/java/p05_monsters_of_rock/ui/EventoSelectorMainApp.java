package p05_monsters_of_rock.ui;

import p05_monsters_of_rock.modelo.Evento;
import p05_monsters_of_rock.stores.EventosStore;
import p05_monsters_of_rock.ui.CollectionConSeleccion;
import p05_monsters_of_rock.ui.SelectorEventoMainWindow;

public class EventoSelectorMainApp {

	public static void main(String[] args) {
		
		EventosStore es = EventosStore.getInstancia();
		es.cargarDatosPrueba();

		CollectionConSeleccion<Evento> ecs = new CollectionConSeleccion<>(es.getEventosOrdenNombre());
		
		SelectorEventoMainWindow esmw = new SelectorEventoMainWindow(ecs);
		esmw.startApplication();

	}
	
}
