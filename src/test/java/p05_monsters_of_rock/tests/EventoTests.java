package p05_monsters_of_rock.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import p05_monsters_of_rock.modelo.Recital;

public class EventoTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	
	private Recital recital = new Recital("Recital", opt.seasonOfMist, 0, 0);
	
	@Before
	public void setUp() throws Exception {

		opt.coloredSands.agregarCopiasVendidas(opt.argentina, 9000);
		opt.syzygialMiscreancy.agregarCopiasVendidas(opt.argentina, 1500);
		opt.chaosAD.agregarCopiasVendidas(opt.argentina, 2900);
		
		opt.coloredSands.agregarCopiasVendidas(opt.canada, 15000);
		opt.syzygialMiscreancy.agregarCopiasVendidas(opt.canada, 2500);
		opt.chaosAD.agregarCopiasVendidas(opt.canada, 4900);
		
	}

	@Test
	public void testEjercicio10parcial_IndiceDeExitoPotencial_SinBandasEnPais() {

		recital.agregarSede(opt.nob);
		
		opt.gorguts.agregarseAEvento(recital, opt.coloredSands, 90);
		opt.hellwitch.agregarseAEvento(recital, opt.syzygialMiscreancy, 45);
		opt.sepultura.agregarseAEvento(recital, opt.chaosAD, 60);
		
		// ninguna banda es del país
		assertEquals(9000 + 1500 + 2900, recital.indiceDeExitoPotencial(), 0);

	}

	@Test
	public void testEjercicio10parcial_IndiceDeExitoPotencial_ConBandasEnPais() {

		recital.agregarSede(opt.molson);
				
		opt.gorguts.agregarseAEvento(recital, opt.coloredSands, 90);
		opt.hellwitch.agregarseAEvento(recital, opt.syzygialMiscreancy, 45);
		opt.sepultura.agregarseAEvento(recital, opt.chaosAD, 60);
		
		// gorguts es de Canadá, Molson está en Canadá => agrega 0.05
		assertEquals((15000 + 2500 + 4900) * 1.05, recital.indiceDeExitoPotencial(), 0);
		
	}

}
