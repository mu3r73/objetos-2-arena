package p05_monsters_of_rock.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import p05_monsters_of_rock.modelo.CenaShow;

public class CenaShowTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	
	private CenaShow cenaShow = new CenaShow("Cena-Show", opt.nuclearBlast, 100, 50000);
	
	@Test
	public void testEjercicio4parcial_PuedeAgregarse() {
		// la compatibilidad con el organizador se verifica en OrganizadorTests
		// todas las bandas en este test son compatibles con el organizador de la cena show

		cenaShow.agregarSede(opt.parque);
		
		// no hay banda, cualquiera puede agregarse
		opt.nuclearBlast.checkPuedeTrabajarCon(opt.symphonyX, opt.newMythologySuite);
		assertTrue(opt.symphonyX.puedeAgregarseA(cenaShow, opt.newMythologySuite, 60));
		
		opt.symphonyX.agregarseAEvento(cenaShow, opt.newMythologySuite, 60);
		
		// ya se agregó una banda => no pueden agregarse más, aunque sean compatibles
		opt.nuclearBlast.checkPuedeTrabajarCon(opt.textures, opt.drawingCircles);
		assertFalse(opt.textures.puedeAgregarseA(cenaShow, opt.drawingCircles, 40));
		
	}
	
	@Test
	public void testEjercicio6_IncluyeGenero() {
		
		cenaShow.agregarSede(opt.parque);
		
		opt.symphonyX.agregarseAEvento(cenaShow, opt.newMythologySuite, 60);
		
		assertTrue(cenaShow.incluyeGenero("rock"));
		assertFalse(cenaShow.incluyeGenero("grunge"));
		
	}

	@Test
	public void testEjercicio8_EventoEconomicamenteFactible() {
		
		cenaShow.agregarSede(opt.bosque);
		
		// antes de agregar banda
		assertTrue(cenaShow.esEconomicamenteFactible());
		
		// agregamos banda
		opt.death.agregarseAEvento(cenaShow, opt.symbolic, 120); // 150000
		
		// ahora no alcanza a financiarse
		assertFalse(cenaShow.esEconomicamenteFactible());
		
		// subimos el precio de la entrada
		cenaShow.setPrecioEntrada(150.0);
		
		// ahora alcanza a financiarse
		assertTrue(cenaShow.esEconomicamenteFactible());

	}
	
	@Test
	public void testEjercicio9parcial_PuedeRealizarseEn() {
		// la compatibilidad con la sede se verifica en OrganizadorTests
		// todos los organizadores en este test son compatibles con la sede de la cena show

		// cena-show no puede realizarse en estadios 
		opt.nuclearBlast.checkPuedeTrabajarEn(opt.nob);
		assertFalse(cenaShow.puedeRealizarseEn(opt.nob));
		
		// cena-show puede realizarse en anfiteatros
		opt.nuclearBlast.checkPuedeTrabajarEn(opt.parque);
		assertTrue(cenaShow.puedeRealizarseEn(opt.parque));
		
	}

}
