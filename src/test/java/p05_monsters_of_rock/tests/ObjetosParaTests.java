package p05_monsters_of_rock.tests;

import p05_monsters_of_rock.modelo.Anfiteatro;
import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Ciudad;
import p05_monsters_of_rock.modelo.Disco;
import p05_monsters_of_rock.modelo.Discografica;
import p05_monsters_of_rock.modelo.Estadio;
import p05_monsters_of_rock.modelo.Pais;
import p05_monsters_of_rock.modelo.Sede;

/**
 * clase con instancias de objetos como atributos públicos
 * (para no tener que definirlos en cada clase de tests) 
 */
public class ObjetosParaTests {

	public Pais alemania = new Pais("Alemania");
	public Pais argentina = new Pais("Argentina");
	public Pais australia = new Pais("Austraila");
	public Pais brasil = new Pais("Brasil");
	public Pais canada = new Pais("Canada");
	public Pais eeuu = new Pais("EEUU");
	public Pais inglaterra = new Pais("Inglaterra"); 
	public Pais italia = new Pais("Italia");
	public Pais noruega = new Pais("Noruega");
	public Pais paisesBajos = new Pais("Países Bajos");
	public Pais rusia = new Pais("Rusia");
	public Pais suecia = new Pais("Suecia");
	
	public Banda death = new Banda("Death", "metal", 150000, eeuu);
	public Banda gorguts = new Banda("Gorguts", "metal", 150000, canada);
	public Banda hellwitch =  new Banda("Hellwitch", "metal", 75000, eeuu);
	public Banda pavor = new Banda("Pavor", "metal", 75000, alemania);
	public Banda sepultura = new Banda("Sepultura", "metal", 80000, brasil);
	public Banda tesseract = new Banda("Tesseract", "djent", 7500, inglaterra);
	
	public Banda cynic = new Banda("Cynic", "rock", 100000, eeuu);
	public Banda dreamTheater = new Banda("Dream Theater", "rock", 120000, eeuu);
	public Banda mastodon = new Banda("Mastodon", "rock", 80000, eeuu);
	public Banda symphonyX = new Banda("Symphony X", "rock", 90000, eeuu);
	public Banda textures = new Banda("Textures", "rock", 50000, paisesBajos);
	
	public Banda aliceInChains = new Banda("Alice in Chains", "grunge", 90000, eeuu);
	public Banda motherLoveBone = new Banda("Mother Love Bone", "grunge", 50000, eeuu);
	public Banda nirvana = new Banda("Nirvana", "grunge", 100000, eeuu);
	public Banda pearlJam = new Banda("Pearl Jam", "grunge", 90000, eeuu);
	public Banda sonicYouth = new Banda("Sonic Youth", "grunge", 80000, eeuu);
	public Banda soundgarden = new Banda("Soundgarden", "grunge", 100000, eeuu);
	public Banda stoneTemplePilots = new Banda("Stone Temple Pilots", "grunge", 90000, eeuu);
	
	public Ciudad bsas = new Ciudad("Buenos Aires", argentina);
	public Ciudad cordoba = new Ciudad("Cordoba", argentina);
	public Ciudad laPlata = new Ciudad("La Plata", argentina);
	public Ciudad rosario = new Ciudad("Rosario", argentina);
	public Ciudad montreal = new Ciudad("Montreal", canada);
	public Ciudad regina = new Ciudad("Regina", canada);
	public Ciudad toronto = new Ciudad("Toronto", canada);
	public Ciudad vancouver = new Ciudad("Vancouver", canada);
	
	public Sede cab = new Estadio("El Gigante de Alberdi", cordoba, 22000);
	public Estadio nob = new Estadio("Estadio Marcelo Bielsa", rosario, 42000);
	public Anfiteatro bosque = new Anfiteatro("Anfiteatro Martín Fierro", laPlata, 2600);
	public Sede parque = new Anfiteatro("Anfiteatro Eva Perón", bsas, 2000);
	public Sede molson = new Anfiteatro("Budweiser Stage", toronto, 16000);
	public Sede mosaic = new Estadio("Mosaic Stadium", regina, 33000);
	public Sede olympique = new Estadio("Stade Olympique", montreal, 56000);

	public Discografica olympic = new Discografica("Olympic Recordings");
	public Discografica nuclearBlast = new Discografica("Nuclear Blast");
	public Discografica seasonOfMist = new Discografica("Season of Mist");
	public Discografica subPop = new Discografica("Sub Pop Records");
	
	public Disco symbolic = new Disco("Symbolic", death, 1995, nuclearBlast);
	public Disco obscura = new Disco("Obscura", gorguts, 1998, olympic);
	public Disco coloredSands = new Disco("Colored Sands", gorguts, 2013, seasonOfMist);
	public Disco syzygialMiscreancy = new Disco("Syzygial Miscreancy", hellwitch, 1986, seasonOfMist);
	public Disco paleDebilitatingAutumn = new Disco("A Pale Debilitating Autumn", pavor, 1994, nuclearBlast);
	public Disco chaosAD = new Disco("Chaos A.D.", sepultura, 1993, seasonOfMist);
	public Disco one = new Disco("One", tesseract, 2010, seasonOfMist);
	
	public Disco tracedInAir = new Disco("Traced in Air", cynic, 2008, nuclearBlast);
	public Disco systematicChaos = new Disco("Systematic Chaos", dreamTheater, 2007, subPop);
	public Disco crackTheSkye = new Disco("Crack the Skye", mastodon, 2009, seasonOfMist);
	public Disco newMythologySuite = new Disco("The New Mythology Suite", symphonyX, 2000, nuclearBlast);
	public Disco drawingCircles = new Disco("Drawing Circles", textures, 2006, nuclearBlast);

	public Disco facelift = new Disco("Facelift", aliceInChains, 1990, subPop);
	public Disco apple = new Disco("Apple", motherLoveBone, 1990, subPop);
	public Disco nevermind = new Disco("Nevermind", nirvana, 1991, subPop);
	public Disco ten = new Disco("Ten", pearlJam, 1991, subPop);
	public Disco goo = new Disco("Goo", sonicYouth, 1990, subPop);
	public Disco superunknown = new Disco("Superunknown", soundgarden, 1994, subPop);
	public Disco core = new Disco("Core", stoneTemplePilots, 1992, subPop);
	
}
