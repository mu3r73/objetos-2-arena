package p05_monsters_of_rock.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import p05_monsters_of_rock.modelo.Recital;

public class RecitalTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	
	private Recital recital = new Recital("Recital", opt.subPop, 0, 50000);
	
	@Before
	public void setUp() throws Exception {

		opt.superunknown.agregarCopiasVendidas(opt.argentina, 10000);
		opt.systematicChaos.agregarCopiasVendidas(opt.argentina, 3000);
		opt.nevermind.agregarCopiasVendidas(opt.argentina, 15000);
		opt.apple.agregarCopiasVendidas(opt.argentina, 500);
		opt.facelift.agregarCopiasVendidas(opt.argentina, 3000);
		opt.ten.agregarCopiasVendidas(opt.argentina, 3000);
		opt.core.agregarCopiasVendidas(opt.argentina, 800);
		
		recital.agregarSede(opt.nob);
		
	}

	@Test
	public void testEjercicio4parcial_PuedeAgregarse() {
		// la compatibilidad con el organizador se verifica en OrganizadorTests
		// todas las bandas en este test son compatibles con el organizador del recital

		// no hay banda, cualquiera puede agregarse
		opt.subPop.checkPuedeTrabajarCon(opt.soundgarden, opt.superunknown);
		assertTrue(opt.soundgarden.puedeAgregarseA(recital, opt.superunknown, 40));
		
		// género = grunge
		opt.soundgarden.agregarseAEvento(recital, opt.superunknown, 50);

		// género = rock, distinto que banda principal
		opt.subPop.checkPuedeTrabajarCon(opt.dreamTheater, opt.systematicChaos);
		assertFalse(opt.dreamTheater.puedeAgregarseA(recital, opt.systematicChaos, 30));
		
		// vendió más que 1/3 de las copias vendidas por la banda principal
		opt.subPop.checkPuedeTrabajarCon(opt.nirvana, opt.nevermind);
		assertFalse(opt.nirvana.puedeAgregarseA(recital, opt.nevermind, 30));
		
		// agregamos 3 bandas compatibles
		opt.subPop.checkPuedeTrabajarCon(opt.motherLoveBone, opt.apple);
		assertTrue(opt.motherLoveBone.puedeAgregarseA(recital, opt.apple, 35));
		
		opt.motherLoveBone.agregarseAEvento(recital, opt.apple, 35);
		
		opt.subPop.checkPuedeTrabajarCon(opt.aliceInChains, opt.facelift);
		assertTrue(opt.aliceInChains.puedeAgregarseA(recital, opt.facelift, 45));

		opt.aliceInChains.agregarseAEvento(recital, opt.facelift, 45);
		
		opt.subPop.checkPuedeTrabajarCon(opt.pearlJam, opt.ten);
		assertTrue(opt.pearlJam.puedeAgregarseA(recital, opt.ten, 45));
		
		opt.pearlJam.agregarseAEvento(recital, opt.ten, 45);
		
		// ya hay 3 bandas soporte => no se puede agregar otra, aunque sea compatible
		opt.subPop.checkPuedeTrabajarCon(opt.stoneTemplePilots, opt.core);
		assertFalse(opt.stoneTemplePilots.puedeAgregarseA(recital, opt.core, 35));
		
	}

	@Test
	public void testEjercicio6_IncluyeGenero() {
		
		opt.soundgarden.agregarseAEvento(recital, opt.superunknown, 50);
		
		assertTrue(recital.incluyeGenero("grunge"));
		assertFalse(recital.incluyeGenero("metal"));
		
	}
	
	@Test
	public void testEjercicio8_EventoEconomicamenteFactible() {
		
		// agregamos sede
		opt.nob.setCostoAlquilerPorHora(10000);

		// antes de agregar bandas
		assertTrue(recital.esEconomicamenteFactible());
		
		// agregamos importe para publicidad
		recital.setImporteParaPublicidad(180000);
		
		// todavía alcanza a financiarse
		assertTrue(recital.esEconomicamenteFactible());

		// agregamos bandas
		
		opt.soundgarden.agregarseAEvento(recital, opt.superunknown, 90); // 100000
		opt.motherLoveBone.agregarseAEvento(recital, opt.apple, 60);
		opt.aliceInChains.agregarseAEvento(recital, opt.facelift, 60);

		// todavía alcanza a financiarse
		assertTrue(recital.esEconomicamenteFactible());

		// agregamos otra banda
		opt.pearlJam.agregarseAEvento(recital, opt.ten, 45);
		
		// ahora ya no alcanza a financiarse
		assertFalse(recital.esEconomicamenteFactible());
				
	}

	@Test
	public void testEjercicio9parcial_PuedeRealizarseEn() {
		// la compatibilidad con la sede se verifica en OrganizadorTests
		// todos los organizadores en este test son compatibles con la sede del recital

		// recital puede realizarse en estadios 
		opt.subPop.checkPuedeTrabajarEn(opt.nob);
		assertTrue(recital.puedeRealizarseEn(opt.nob));
		
		// recital puede realizarse en anfiteatros
		opt.subPop.checkPuedeTrabajarEn(opt.parque);
		assertTrue(recital.puedeRealizarseEn(opt.parque));
		
	}
	
}
