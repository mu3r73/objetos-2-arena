package p05_monsters_of_rock.tests;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.Evento;
import p05_monsters_of_rock.modelo.Festival;
import p05_monsters_of_rock.modelo.Recital;

public class SedeTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	
	private Festival festival = new Festival("Festival", opt.seasonOfMist, 0, 0);
	private Evento recital = new Recital("Recital", opt.nuclearBlast, 0, 0); 
	
	@Before
	public void setUp() throws Exception {
		
		festival.agregarSede(opt.nob);		
		recital.agregarSede(opt.cab);

		opt.syzygialMiscreancy.agregarCopiasVendidas(opt.argentina, 1000);
		opt.obscura.agregarCopiasVendidas(opt.argentina, 5000);
		opt.chaosAD.agregarCopiasVendidas(opt.argentina, 800);
		
		opt.tracedInAir.agregarCopiasVendidas(opt.argentina, 8000);
		opt.newMythologySuite.agregarCopiasVendidas(opt.argentina, 2000);
		
		festival.agregarGeneroPermitido("metal");
		
		opt.gorguts.agregarseAEvento(festival, opt.obscura, 45);
		opt.sepultura.agregarseAEvento(festival, opt.chaosAD, 35);
		opt.hellwitch.agregarseAEvento(festival, opt.syzygialMiscreancy, 40);
		
		opt.cynic.agregarseAEvento(recital, opt.tracedInAir, 40);
		opt.symphonyX.agregarseAEvento(recital, opt.newMythologySuite, 30);
				
	}
	
	@Test
	public void testEjercicio3_BandasQueParticipanEnEventosEnSede() {
		
		Set<Banda> resEsperado = new HashSet<>(Arrays.asList(opt.hellwitch, opt.gorguts, opt.sepultura));
		
		assertEquals(opt.nob.bandasQueParticipanEnEventosEnSede(), resEsperado);
		
		resEsperado = new HashSet<>(Arrays.asList(opt.cynic, opt.symphonyX));
		
		assertEquals(opt.cab.bandasQueParticipanEnEventosEnSede(), resEsperado);
		
	}

}
