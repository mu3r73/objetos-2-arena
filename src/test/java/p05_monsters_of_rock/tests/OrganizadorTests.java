package p05_monsters_of_rock.tests;

import static org.junit.Assert.fail;

import org.junit.Test;

public class OrganizadorTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	
	@Test
	public void testEjercicio4parcial_PuedeTrabajarCon() {

		// organiza ciudad Vancouver, banda es de Canadá
		opt.vancouver.checkPuedeTrabajarCon(opt.gorguts, opt.obscura);
		
		// organiza ciudad Rosario, banda no es de Argentina
		try {
			opt.rosario.checkPuedeTrabajarCon(opt.gorguts, opt.obscura);
			fail();
		} catch (RuntimeException e) {
			
		}
		
		// organiza discográfica seasonOfMist,
		// banda presenta disco de discográfica seasonOfMist
		opt.seasonOfMist.checkPuedeTrabajarCon(opt.gorguts, opt.coloredSands);
		
		// organiza discográfica seasonOfMist,
		// banda presenta disco de discográfica olympicRecordings,
		// pero seasonOfMist produjo su último disco
		opt.seasonOfMist.checkPuedeTrabajarCon(opt.gorguts, opt.obscura);
		
		// organiza discográfica olympicRecordings,
		// banda presenta disco de discográfica seasonOfMist,
		// y olympicRecordings no produjo su último disco
		try {
			opt.olympic.checkPuedeTrabajarCon(opt.gorguts, opt.coloredSands);
			fail();
		} catch (RuntimeException e) {
			
		}
		
	}
	
	@Test
	public void testEjercicio9parcial_PuedeTrabajarEn() {
		
		// discográfica puede organizar eventos en cualquier sede
		opt.seasonOfMist.checkPuedeTrabajarEn(opt.nob);
		opt.seasonOfMist.checkPuedeTrabajarEn(opt.molson);
		
		// ciudad sólo puede organizar eventos en ciudades del mismo país
		opt.bsas.checkPuedeTrabajarEn(opt.nob);
		
		try {
			opt.bsas.checkPuedeTrabajarEn(opt.molson);
			fail();
		} catch (RuntimeException e) {
			
		}
		
	}

}
