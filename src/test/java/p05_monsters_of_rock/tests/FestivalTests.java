package p05_monsters_of_rock.tests;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import p05_monsters_of_rock.modelo.Festival;


public class FestivalTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	
	private Festival festival1 = new Festival("Festival 1", opt.seasonOfMist, 0, 100000);
	private Festival festival2 = new Festival("Festival 2", opt.nuclearBlast, 0, 100000);
	
	@Before
	public void setUp() throws Exception {

		festival1.agregarSede(opt.nob);
		festival2.agregarSede(opt.nob);
		
		festival1.agregarGeneroPermitido("metal");
		festival1.agregarGeneroPermitido("rock");
		
		festival2.agregarGeneroPermitido("metal");
		festival2.agregarGeneroPermitido("rock");
		
		// para festival1
		opt.obscura.agregarCopiasVendidas(opt.argentina, 5000);
		opt.crackTheSkye.agregarCopiasVendidas(opt.argentina, 3000);
		opt.chaosAD.agregarCopiasVendidas(opt.argentina, 1000);
		opt.syzygialMiscreancy.agregarCopiasVendidas(opt.argentina, 8000);
		opt.superunknown.agregarCopiasVendidas(opt.argentina, 4000);
		
		// para festival2
		opt.symbolic.agregarCopiasVendidas(opt.argentina, 3000);
		opt.paleDebilitatingAutumn.agregarCopiasVendidas(opt.argentina, 1500);
		opt.tracedInAir.agregarCopiasVendidas(opt.argentina, 2800);
		opt.drawingCircles.agregarCopiasVendidas(opt.argentina, 800);
		opt.newMythologySuite.agregarCopiasVendidas(opt.argentina, 1500);
		
	}

	@Test
	public void testEjercicio4parcial_PuedeAgregarse() {
		// la compatibilidad con el organizador se verifica en OrganizadorTests
		// todas las bandas en este test son compatibles con el organizador del festival

		// género permitido, no excede duración máxima, no hay banda de cierre
		opt.seasonOfMist.checkPuedeTrabajarCon(opt.gorguts, opt.obscura);
		assertTrue(opt.gorguts.puedeAgregarseA(festival1, opt.obscura, 40));
		
		opt.gorguts.agregarseAEvento(festival1, opt.obscura, 40);

		// género permitido, duración ok, ventas ok
		opt.seasonOfMist.checkPuedeTrabajarCon(opt.mastodon, opt.crackTheSkye);
		assertTrue(opt.mastodon.puedeAgregarseA(festival1, opt.crackTheSkye, 20));
		
		// género no permitido (djent, espera metal o rock)
		opt.seasonOfMist.checkPuedeTrabajarCon(opt.tesseract, opt.goo);
		assertFalse(opt.tesseract.puedeAgregarseA(festival1, opt.one, 30));
		
		// presentación excede duración máxima
		opt.seasonOfMist.checkPuedeTrabajarCon(opt.sepultura, opt.chaosAD);
		assertFalse(opt.sepultura.puedeAgregarseA(festival1, opt.chaosAD, 13 * 60));
		
		// vendió más copias que la banda de cierre
		opt.seasonOfMist.checkPuedeTrabajarCon(opt.hellwitch, opt.syzygialMiscreancy);
		assertFalse(opt.hellwitch.puedeAgregarseA(festival1, opt.syzygialMiscreancy, 30));
		
	}
	
	@Test
	public void testEjercicio6_IncluyeGenero() {
	
		opt.death.agregarseAEvento(festival2, opt.symbolic, 120);
		opt.pavor.agregarseAEvento(festival2, opt.paleDebilitatingAutumn, 90);
		opt.cynic.agregarseAEvento(festival2, opt.tracedInAir, 60);
		opt.textures.agregarseAEvento(festival2, opt.drawingCircles, 45);
		opt.symphonyX.agregarseAEvento(festival2, opt.newMythologySuite, 60);
		
		// género de la banda principal
		assertTrue(festival2.incluyeGenero("metal"));
		
		// género de 3 bandas soporte
		assertTrue(festival2.incluyeGenero("rock"));
		
		assertFalse(festival2.incluyeGenero("grunge"));
		
	}
	
	@Test
	public void testEjercicio8_EventoEconomicamenteFactible() {

		// antes de agregar bandas / auspiciantes
		assertTrue(festival1.esEconomicamenteFactible());

		// agregamos bandas
		opt.gorguts.agregarseAEvento(festival1, opt.obscura, 90); // 150000
		opt.mastodon.agregarseAEvento(festival1, opt.crackTheSkye, 60); // 80000
		opt.sepultura.agregarseAEvento(festival1, opt.chaosAD, 60); // 80000
		
		// ahora no alcanza a financiarse solamente con el monto inicial
		assertFalse(festival1.esEconomicamenteFactible());
		
		// agregamos auspiciantes
		festival1.agregarAporte("Coca-Cola", 50000);
		festival1.agregarAporte("Cerveza Quilmes", 50000);
		festival1.agregarAporte("Ford", 50000);
		
		// ahora sí alcanza a financiarse
		assertTrue(festival1.esEconomicamenteFactible());
		
	}

	@Test
	public void testEjercicio9parcial_PuedeRealizarseEn() {
		// la compatibilidad con la sede se verifica en OrganizadorTests
		// todos los organizadores en este test son compatibles con la sede del festival

		// recital puede realizarse en estadios 
		opt.seasonOfMist.checkPuedeTrabajarEn(opt.nob);
		assertTrue(festival1.puedeRealizarseEn(opt.nob));
		
		// recital no puede realizarse en anfiteatros
		opt.seasonOfMist.checkPuedeTrabajarEn(opt.parque);
		assertFalse(festival1.puedeRealizarseEn(opt.parque));
		
	}
	
}
