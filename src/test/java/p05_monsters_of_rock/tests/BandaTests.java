package p05_monsters_of_rock.tests;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import p05_monsters_of_rock.modelo.Evento;
import p05_monsters_of_rock.modelo.Recital;;

public class BandaTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	private Evento recital = new Recital("Recital", opt.seasonOfMist, 0, 0);
	
	@Before
	public void setUp() throws Exception {

		opt.obscura.agregarCopiasVendidas(opt.argentina, 9);
		opt.obscura.agregarCopiasVendidas(opt.canada, 140);
		opt.obscura.agregarCopiasVendidas(opt.noruega, 68);
		opt.obscura.agregarCopiasVendidas(opt.rusia, 62);
		
		opt.coloredSands.agregarCopiasVendidas(opt.argentina, 17);
		opt.coloredSands.agregarCopiasVendidas(opt.australia, 34);
		opt.coloredSands.agregarCopiasVendidas(opt.canada, 200);
		opt.coloredSands.agregarCopiasVendidas(opt.rusia, 57);
		opt.coloredSands.agregarCopiasVendidas(opt.suecia, 79);
		
		recital.agregarSede(opt.nob);

	}
	
	@Test
	public void testEjercicio1_CuantasCopiasVendio() {
		
		assertEquals(666, opt.gorguts.getTotalCopiasVendidas());
		
	}

	@Test
	public void testEjercicio2_UltimoDiscoEditado() {
		
		assertEquals(opt.coloredSands, opt.gorguts.ultimoDisco());
		
	}
	
	@Test
	public void testEjercicio5parcial_AgregarBandaAEvento_SePuedeAgregar() {
		
		// banda compatible con evento
		opt.gorguts.agregarseAEvento(recital, opt.obscura, 60);
		
		assertTrue(recital.todasLasBandas().contains(opt.gorguts));
		
	}
	
	@Test(expected = RuntimeException.class)
	public void testEjercicio5parcial_AgregarBandaAEvento_NoSePuedeAgregar() {
		
		// banda incompatible con evento
		opt.sonicYouth.agregarseAEvento(recital, opt.goo, 45);
		
	}
	
}
