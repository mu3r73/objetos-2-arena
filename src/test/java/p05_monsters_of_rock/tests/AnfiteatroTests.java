package p05_monsters_of_rock.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import p05_monsters_of_rock.modelo.Evento;
import p05_monsters_of_rock.modelo.Recital;

public class AnfiteatroTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	private Evento recital = new Recital("Recital", opt.seasonOfMist, 0, 0);
	
	@Before
	public void setUp() throws Exception {

		opt.obscura.agregarCopiasVendidas(opt.argentina, 8000);
		opt.tracedInAir.agregarCopiasVendidas(opt.argentina, 2500);
		
		opt.bosque.setCostoPorEvento(8000);
		
		recital.agregarSede(opt.bosque);
		
	}
	
	@Test
	public void testEjercicio7_CostoDeAlquiler() {
	
		opt.gorguts.agregarseAEvento(recital, opt.obscura, 120);
		
		assertEquals(8000, recital.costoAlquilerDeSede(), 0);
		
		opt.bosque.agregarGeneroEnPromocion("metal");
		
		assertEquals(8000 * 0.8, recital.costoAlquilerDeSede(), 0);
		
	}

}
