package p05_monsters_of_rock.tests;

import static org.junit.Assert.assertEquals;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import p05_monsters_of_rock.modelo.Banda;
import p05_monsters_of_rock.modelo.CenaShow;
import p05_monsters_of_rock.modelo.Evento;
import p05_monsters_of_rock.modelo.Festival;
import p05_monsters_of_rock.modelo.Recital;

public class PaisTests {

	private ObjetosParaTests opt = new ObjetosParaTests();

	Festival festival = new Festival("Festival", opt.nuclearBlast, 0, 100000);
	Evento recital = new Recital("Recital", opt.subPop, 0, 0);
	Evento cenaShow = new CenaShow("Cena-Show", opt.seasonOfMist, 100, 50000);
	
	@Before
	public void setUp() throws Exception {

		festival.agregarSede(opt.mosaic);
		recital.agregarSede(opt.olympique);
		cenaShow.agregarSede(opt.molson);
		
		opt.superunknown.agregarCopiasVendidas(opt.alemania, 1500);
		
		opt.soundgarden.agregarseAEvento(recital, opt.superunknown, 90);
		opt.motherLoveBone.agregarseAEvento(recital, opt.apple, 60);
		opt.aliceInChains.agregarseAEvento(recital, opt.facelift, 60);

		festival.agregarGeneroPermitido("metal");
		festival.agregarGeneroPermitido("rock");

		opt.symbolic.agregarCopiasVendidas(opt.australia, 1500);
		
		opt.death.agregarseAEvento(festival, opt.symbolic, 120);
		opt.pavor.agregarseAEvento(festival, opt.paleDebilitatingAutumn, 90);
		opt.cynic.agregarseAEvento(festival, opt.tracedInAir, 60);
		opt.textures.agregarseAEvento(festival, opt.drawingCircles, 45);
		opt.symphonyX.agregarseAEvento(festival, opt.newMythologySuite, 60);
		
		opt.gorguts.agregarseAEvento(cenaShow, opt.obscura, 90);
		
	}

	@Test
	public void test() {

		// 3 eventos en Canadá, participan 9 bandas, 1 de ellas es canadiense
		Set<Banda> bExtranj = opt.canada.bandasExtranjerasQueParticipanEnEventos();
		
		Set<Banda> resultEsperado = new HashSet<>(Arrays.asList(
				opt.soundgarden, opt.motherLoveBone, opt.aliceInChains, opt.death,
				opt.pavor, opt.cynic, opt.textures, opt.symphonyX));
		
		assertEquals(bExtranj, resultEsperado);
		
	}

}
