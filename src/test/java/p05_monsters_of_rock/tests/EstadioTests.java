package p05_monsters_of_rock.tests;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import p05_monsters_of_rock.modelo.Evento;
import p05_monsters_of_rock.modelo.Recital;

public class EstadioTests {

	private ObjetosParaTests opt = new ObjetosParaTests();
	private Evento recital = new Recital("Recital", opt.seasonOfMist, 0, 0);
	
	@Before
	public void setUp() throws Exception {

		opt.obscura.agregarCopiasVendidas(opt.argentina, 8000);
		opt.chaosAD.agregarCopiasVendidas(opt.argentina, 1500);
		
		opt.nob.setCostoAlquilerPorHora(10000);
		
		recital.agregarSede(opt.nob);
		
	}
	
	@Test
	public void testEjercicio7_CostoDeAlquiler() {
		
		opt.gorguts.agregarseAEvento(recital, opt.obscura, 120);
		
		assertEquals(10000 * 120 / 60, recital.costoAlquilerDeSede(), 0);
		
		opt.sepultura.agregarseAEvento(recital, opt.chaosAD, 90);
		
		assertEquals(10000 * 210 / 60, recital.costoAlquilerDeSede(), 0);
		
	}

}
